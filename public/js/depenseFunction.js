// retourne les depenses par mois
function depenseByMonth(dates, month, year){
    return dates.map(depense => {
        if (new Date(depense.date).getMonth() === month &&
            new Date(depense.date).getFullYear() === year){
            return {
                date: depense.date.toLocaleString(),
                montant: depense.montant
            };
        }
        return null;
    }).filter(depense => depense !== null)
}

// reécupérer les montants des depenses mensuelles par année
function depenseByYear(dates,year){
    let montantByMonth = [];
    allMonth().forEach((month, index) =>{
        let montantMonth = dates.map(depense => {
            if (new Date(depense.date).getUTCMonth() === index &&
                new Date(depense.date).getFullYear() === year)
                return depense.montant;
            return 0;
        }).reduce(function (total,montant) {
            return total + montant;
        },0);

        montantByMonth.push({
            month: month,
            montant: montantMonth
        });
    });
    return montantByMonth;
}

function montantAnnuelle(year) {
    const depenseYear =  depenseByYear(dateDepenses, year).map(depense => {
        return depense.montant;
    });
    const montantAn = depenseYear.reduce(function (total,montant) {
        return total + parseInt(montant);
    }, 0);

    $('#montant_annuel').text(formatNumber(montantAn));
}

function montantDepenseAnnuelle(year) {
    const depenseYear =  depenseByYear(dateDepenses, year).map(depense => {
        return depense.montant;
    });
    return depenseYear.reduce(function (total,montant) {
        return total + parseInt(montant);
    }, 0);

}