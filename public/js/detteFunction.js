function montantDetteAnnuelle(year) {
    const detteYear =  detteByYear(dateDettes, year).map(dette => {
        return dette.montant;
    });
    return detteYear.reduce(function (total,montant) {
        return total + parseInt(montant);
    }, 0);

}

// reécupérer les montants des dettes mensuelles par année
function detteByYear(dates,year){
    let montantByMonth = [];
    allMonth().forEach((month, index) =>{
        let montantMonth = dates.map(dette => {
            if (new Date(dette.date).getUTCMonth() === index &&
                new Date(dette.date).getFullYear() === year)
                return dette.montant;
            return 0;
        }).reduce(function (total,montant) {
            return total + montant;
        },0);

        montantByMonth.push({
            month: month,
            montant: montantMonth
        });
    });
    return montantByMonth;
}