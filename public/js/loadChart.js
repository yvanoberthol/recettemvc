// methode pour mettre à jour la vue du tableau statisque en fonction des critères
function loadAreaChart(ctxElem, datas){
    Chart.defaults.global.defaultFontFamily='-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor="#292b2c";

    const montantArray = datas.map(object => {return object.montant});

    const ticketValue = 4;
    let pourcent = 0;

    let color = "rgb(43,153,216)";
    if (montantArray.length > 0){
        color = montantArray.map(x => 'rgb(43,153,216)');
        color[argMax(montantArray)] = 'rgb(2,3,216)';
        color[argMin(montantArray)] = 'rgb(216,22,31)';

        const valueMax = montantArray[argMax(montantArray)];
        pourcent = pourcentageLimitChart(valueMax);
    }

    let ctx = document.getElementById(ctxElem).getContext('2d');

    return new Chart(ctx,{
        type:"line",
        data:{
            labels: datas.map(object => {return object.date}),
            datasets:[
                {
                    label:"Recette",
                    lineTension:.3,
                    backgroundColor:"rgba(2,117,216,0.2)",
                    borderColor:"rgba(2,117,216,1)",
                    pointRadius:5,
                    pointBackgroundColor: color,
                    pointBorderColor:"rgba(255,237,48,0.8)",
                    pointHoverRadius:5,
                    pointHoverBackgroundColor:"rgba(2,117,216,1)",
                    pointHitRadius:20,
                    pointBorderWidth:2,
                    data: montantArray
                }
            ]
        },
        options: {
            scales:
                {
                    xAxes:
                        [
                            {
                                time:{unit:"date"},
                                gridLines:{display:!1},
                                ticks:{maxTicksLimit:7}
                            }],
                    yAxes:
                        [
                            {
                                ticks:{min:0,max:pourcent,maxTicksLimit:ticketValue,
                                    callback: function(value){
                                        return formatNumber(value);
                                    }},
                                gridLines:{color:"rgba(0, 0, 0, .125)"}
                            }
                        ]
                },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        let label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label){
                            label += ': ';
                        }
                        label += formatNumber(tooltipItem.yLabel);
                        return label;
                    }
                }
            },
            legend:
                {
                    display:!1
                }
        }
    });
}

function loadBarChart(ctxElem, datas){
    Chart.defaults.global.defaultFontFamily='-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor="#292b2c";

    let ctx = document.getElementById(ctxElem).getContext('2d');

    let montantArray = datas.map(object => {
        return object.montant;
    });

    const ticketValue = 4;
    let pourcent = 0;
    if (montantArray.length > 0){
        const valueMax = montantArray[argMax(montantArray)];
        pourcent = pourcentageLimitChart(valueMax);
    }

    return new Chart(ctx,
        {
            type:"bar",
            data:{
                labels:allMonth(),
                datasets:[
                    {
                        label:"Recette",
                        backgroundColor:[
                            "rgba(2,117,216,1)",
                            "rgb(74,216,178)",
                            "rgb(216,170,96)",
                            "rgb(68,216,75)",
                            "rgb(102,64,216)",
                            "rgb(207,210,216)",
                            "rgb(172,138,216)",
                            "rgb(216,83,39)",
                            "rgb(154,216,14)",
                            "rgb(216,11,87)",
                            "rgb(208,216,59)",
                            "rgb(138,201,216)",
                        ],
                        borderColor:"rgba(2,117,216,1)",
                        data:datas.map(object => {
                            return object.montant;
                        })
                    }
                ]
            },
            options:{
                scales:{
                    xAxes:[
                        {
                            time:{unit:"month"},
                            gridLines:{display:!1},
                            ticks:{maxTicksLimit:12, fontSize: 12}
                        }],
                    yAxes:[
                        {
                            ticks:{
                                min:0,
                                max:pourcent,
                                maxTicksLimit:ticketValue,
                                fontSize: 12,
                                callback: function(value){
                                    return formatNumber(value);
                                }},
                            gridLines:{display:!0}
                        }]
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            let label = data.datasets[tooltipItem.datasetIndex].label || '';

                            if (label){
                                label += ': ';
                            }
                            label += formatNumber(tooltipItem.yLabel);
                            return label;
                        }
                    }
                },
                legend:{display:!1}
            }
        });
}

function loadChart(ctxElem,montantArray){
    Chart.defaults.global.defaultFontFamily='-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    Chart.defaults.global.defaultFontColor="#292b2c";
    Chart.defaults.global.scaleFontSize = 600;

    let ctx=document.getElementById(ctxElem).getContext('2d');

    const ticketValue = 4;
    let pourcent = 0;

    if (montantArray.length > 0){
        const valueMax = montantArray[argMax(montantArray)];
        pourcent = pourcentageLimitChart(valueMax);
    }

    return new Chart(ctx,
        {
            type:"horizontalBar",
            data:{
                labels:['Recette', 'Dépense','Dette'],
                datasets:[
                    {
                        label:"montant total",
                        backgroundColor:[
                            "#007bff",
                            "#ffc107",
                            "#17a2b8"
                        ],
                        borderColor:"rgba(2,117,216,1)",
                        data: montantArray
                    }
                ]
            },
            options:{
                scales:{
                    xAxes:[
                        {
                            time:{unit:"month"},
                            gridLines:{display:!1},
                            ticks:{
                                min:0,
                                max:pourcent,
                                maxTicksLimit:ticketValue,
                                fontSize: 20,
                                callback: function(value){
                                    return formatNumber(value);
                                }
                            }
                        }],
                    yAxes:[
                        {
                            ticks: {
                                maxTicksLimit: 3,
                                fontSize: 20
                            },
                            gridLines: {display: !0}
                        }]
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            let label = data.datasets[tooltipItem.datasetIndex].label || '';

                            if (label){
                                label += ': ';
                            }
                            label += formatNumber(tooltipItem.xLabel);
                            return label;
                        }
                    }
                },
                legend:{display:!1}
            }
        });
}