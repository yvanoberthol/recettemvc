function pourcentageLimitChart(value,subvalue = 2) {
    const valueOverMaxValue = 10;
    const charMesureMultiple = 5;
    const lengthValue = value.toString().length;

    let zeroString = '';
    for (let i = 0; i < (lengthValue - subvalue); i++){
        zeroString += '0';
    }

    const numberMultiplier = '1'+zeroString;

    const valueToSub = value.toString().substring(0,subvalue);

    let valueSubEntier = parseInt(valueToSub);

    if (valueSubEntier % charMesureMultiple !== 0){
        valueSubEntier = (valueSubEntier - (valueSubEntier % charMesureMultiple)) + valueOverMaxValue;
    }

    let valueMultiplier = valueSubEntier * parseInt(numberMultiplier);

    if (value >= valueMultiplier){
        valueMultiplier = (valueSubEntier + valueOverMaxValue) * parseInt(numberMultiplier)
    }

    return valueMultiplier;
}