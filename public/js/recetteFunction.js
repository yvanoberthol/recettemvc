// retourne les recettes par mois
function recetteByMonth(dates, month, year){
    return dates.map(recette => {
        if (new Date(recette.date).getMonth() === month &&
            new Date(recette.date).getFullYear() === year){
            return {
                date: recette.date.toLocaleString(),
                montant: recette.montant
            };
        }
        return null;
    }).filter(recette => recette !== null)
}

// reécupérer les montants des recettes mensuelles par année
function recetteByYear(dates,year){
    let montantByMonth = [];
    allMonth().forEach((month, index) =>{
        let montantMonth = dates.map(recette => {
            if (new Date(recette.date).getUTCMonth() === index &&
                new Date(recette.date).getFullYear() === year)
                return recette.montant;
            return 0;
        }).reduce(function (total,montant) {
            return total + montant;
        },0);

        montantByMonth.push({
            month: month,
            montant: montantMonth
        });
    });
    return montantByMonth;
}

function montantAnnuelle(year) {
    const recetteYear =  recetteByYear(dateRecettes, year).map(recette => {
        return recette.montant;
    });
    const montantAn = recetteYear.reduce(function (total,montant) {
        return total + parseInt(montant);
    }, 0);

    $('#montant_annuel').text(formatNumber(montantAn));
}

function montantRecetteAnnuelle(year) {
    const recetteYear =  recetteByYear(dateRecettes, year).map(recette => {
        return recette.montant;
    });
    return recetteYear.reduce(function (total,montant) {
        return total + parseInt(montant);
    }, 0);

}