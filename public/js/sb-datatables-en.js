/*!
 * Start Bootstrap - SB Admin v4.0.0-beta.2 (https://startbootstrap.com/template-overviews/sb-admin)
 * Copyright 2013-2017 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin/blob/master/LICENSE)
 */
let table = $("#dataTable").DataTable({
    language: {
        searchPlaceholder: "Search",
        sLengthMenu: "_MENU_ elements",
        sEmptyTable: "No data found in the table",
        sInfo: "Display of _START_ à _END_ de _TOTAL_ records",
        sInfoFiltered: "(Filtered from _MAX_ total entries)",
        sInfoPostFix: "",
        sInfoThousands: " ",
        sLoadingRecords: "Loading...",
        sSearch: "Search:",
        sZeroRecords: "No matching record",
        oPaginate: {
            sFirst: "First",
            sLast: "Last",
            sNext: "Next",
            sPrevious: "Previous"
        },
        oAria: {
            sSortAscending: "Sorting from bottom to top",
            sSortDescending: "Sorting from top to bottom"
        }
    },
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'copy',
            text: 'Copy',
            className: 'copyButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'csv',
            text: 'CSV',
            className: 'csvButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'excel',
            text: 'Excel',
            className: 'excelButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'pdf',
            text: 'PDF',
            className: 'pdfButton',
            download: 'open',
            exportOptions: {
                columns: ':not(.not-export)'
            },
            customize: function(doc) {
                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                doc.content[1].alignment = 'center';

            }
        },
        {
            extend: 'print',
            text: 'Print',
            className: 'printButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'colvis',
            text:'Visible column',
            className: 'colvisButton',
            collectionLayout: 'fixed two-column'
        }
    ],
    columnDefs: [
        {
            targets: -1,
            className: 'dt-body-center'
        }
    ],
    ordering: false
});