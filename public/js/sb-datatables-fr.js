/*!
 * Start Bootstrap - SB Admin v4.0.0-beta.2 (https://startbootstrap.com/template-overviews/sb-admin)
 * Copyright 2013-2017 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin/blob/master/LICENSE)
 */
let table = $("#dataTable").DataTable({
    language: {
        searchPlaceholder: "Rechercher",
        sLengthMenu: "_MENU_ elements",
        sEmptyTable: "Aucune donnée trouvée dans la table",
        sInfo: "Affichage de _START_ à _END_ de _TOTAL_ enregistrements",
        sInfoFiltered: "(Filtré de _MAX_ entrées total)",
        sInfoPostFix: "",
        sInfoThousands: " ",
        sLoadingRecords: "Chargement...",
        sSearch: "Recherche:",
        sZeroRecords: "Aucun enregistrement correspondant",
        oPaginate: {
            sFirst: "Premier",
            sLast: "Dernier",
            sNext: "Suivant",
            sPrevious: "Précédent"
        },
        oAria: {
            sSortAscending: "Tri du bas vers le haut",
            sSortDescending: "Tri du haut vers le bas"
        }
    },
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'copy',
            text: 'Copier',
            className: 'copyButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'csv',
            text: 'CSV',
            className: 'csvButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'excel',
            text: 'Excel',
            className: 'excelButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'pdf',
            text: 'PDF',
            className: 'pdfButton',
            download: 'open',
            exportOptions: {
                columns: ':not(.not-export)'
            },
            customize: function(doc) {
                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                doc.content[1].alignment = 'center';

            }
        },
        {
            extend: 'print',
            text: 'Imprimer',
            className: 'printButton',
            exportOptions: {
                columns: ':not(:last-child)'
            }
        },
        {
            extend: 'colvis',
            text:'Colonne visible',
            className: 'colvisButton',
            collectionLayout: 'fixed two-column'
        }
    ],
    columnDefs: [
        {
            targets: -1,
            className: 'dt-body-center'
        }
    ],
    ordering: false
});