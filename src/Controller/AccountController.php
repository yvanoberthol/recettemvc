<?php

namespace App\Controller;


use App\Entity\PasswordUpdate;
use App\Entity\Utilisateur;
use App\Form\AccountType;
use App\Form\PasswordUpdateType;
use App\Repository\EntrepriseRepository;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Doctrine\ORM\EntityManagerInterface;
use PragmaRX\Google2FA\Google2FA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/admin")
 */
class AccountController extends AbstractController
{

    /**
     * @Route("/login", name="account_connexion")
     * @param AuthenticationUtils $authenticationUtils
     * @param EntrepriseRepository $entrepriseRepository
     * @return Response
     */
    public function connexion(AuthenticationUtils $authenticationUtils,
                              EntrepriseRepository $entrepriseRepository): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')){
            return $this->redirectToRoute('accueil');
        }

        $model['error'] = $authenticationUtils->getLastAuthenticationError();
        $model['username'] = $authenticationUtils->getLastUsername();
        $model['title'] = 'login.title';
        $model['entreprises'] = $entrepriseRepository->findAll();
        return $this->render('admin/account/login.html.twig', $model);
    }

    /**
     * @Route("/logout", name="account_logout")
     */
    public function logout(){}

    /**
     * @Route("/account/resetpassword", name="account_reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function resetPassword(Request $request,
                                  UserPasswordEncoderInterface $passwordEncoder,
                                  EntityManagerInterface $entityManager){
        $passwordUpdate = new PasswordUpdate();
        $utilisateur = $this->getUser();
        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid() && isset($utilisateur)) {
                if (password_verify($passwordUpdate->getOldpassword(), $utilisateur->getPassword())) {
                    $newpassword = $passwordEncoder->encodePassword($utilisateur, $passwordUpdate->getNewpassword());
                    $utilisateur->setPassword($newpassword);
                    $entityManager->persist($utilisateur);
                    $entityManager->flush();
                    $this->addFlash('success', 'flash.account.update_password');
                    return $this->redirectToRoute('account_logout');
                }

                $form->get('oldpassword')->addError(new FormError('flash.account.incorrect_old_password'));
            }
        }

        $model['form'] = $form->createView();
        $model['title'] = "password.title";
        return $this->render('admin/account/passwordreset.html.twig', $model);
    }

    /**
     * @Route("/account/profile", name="account_profile")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function profile(Request $request,
                            EntityManagerInterface $entityManager){

        /** @var Utilisateur $utilisateur */
        $utilisateur = $this->getUser();
        if ($utilisateur->getGoogleAuthenticatorSecret()) {
            $google2fa = new Google2FA();
            $model['svg'] = $this->generateSvgForUser($google2fa, $utilisateur, $utilisateur->getGoogleAuthenticatorSecret());
        }

        $form = $this->createForm(AccountType::class, $utilisateur);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'flash.account.update_info');
                return $this->redirectToRoute('account_logout');
            }
        }
        $model['form'] = $form->createView();
        $model['title'] = "profile.title";
        return $this->render('admin/account/profile.html.twig', $model);
    }

    private function generateSvgForUser(Google2FA $google2FA, Utilisateur $utilisateur, string $secret): string
    {
        $g2faUrl = $google2FA->getQRCodeUrl(
            "Recetta",
            $utilisateur->getUsername(),
            $secret
        );

        $writer = new Writer(
            new ImageRenderer(
                new RendererStyle(200),
                new SvgImageBackEnd() // can also user new ImagickImageBackEnd() in order to generate png
            )
        );

        return $writer->writeString($g2faUrl);
    }

}
