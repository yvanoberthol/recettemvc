<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Repository\EntrepriseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/client", name="client_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {

        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise)
            return $this->redirectToRoute('account_logout');
        $model['clients'] = $entreprise->getClients();
        $model['title'] = 'client.index.title';


        return $this->render('admin/client/index.html.twig', $model);
    }

    /**
     * @Route("/client/new", name="client_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        SessionInterface $session,
                        Request $request,
                        EntityManagerInterface $entityManager): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise)
            return $this->redirectToRoute('account_logout');

        $client = new Client();

        $form = $this->createForm(ClientType::class,$client);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $client->setEntreprise($entreprise);
                $entityManager->persist($client);
                $entityManager->flush();

                return $this->redirectToRoute('client_index');
            }
        }

        $model['title'] = 'client.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/client/new.html.twig',$model);
    }

    /**
     * @Route("/client/{id}/edit", name="client_edit")
     * @param Client $client
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Client $client, Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ClientType::class,$client);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($client);
                $entityManager->flush();

                return $this->redirectToRoute('client_index');
            }
        }

        $model['title'] = 'client.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/client/edit.html.twig',$model);
    }


    /**
     * @Route("/client/{id}/delete", name="client_delete")
     * @param Client $client
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Client $client, EntityManagerInterface $entityManager): Response
    {
        if (!$client){
            throw new NotFoundHttpException("Aucun client avec l'ID ".$client->getId());
        }
        $entityManager->remove($client);
        $entityManager->flush();

        return $this->redirectToRoute('depense_index');
    }
}
