<?php

namespace App\Controller;

use App\Entity\Depense;
use App\Form\DepenseType;
use App\Repository\DepenseRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class DepenseController extends AbstractController
{
    /**
     * @Route("/depense", name="depense_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {

        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise)
            return $this->redirectToRoute('account_logout');

        $model['depenses'] = $entreprise->getDepenses();
        $model['title'] = 'depense.index.title';


        return $this->render('admin/depense/index.html.twig', $model);
    }

    /**
     * @Route("/depense/new", name="depense_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param ProduitRepository $produitRepository
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        ProduitRepository $produitRepository,
                        SessionInterface $session,
                        Request $request,
                        EntityManagerInterface $entityManager): Response
    {
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $produits = $produitRepository->findOwnedBy($session->get('entreprise'));
        if (empty($produits)) {
            return $this->redirectToRoute('produit_new');
        }



        $depense = new Depense();
        $form = $this->createForm(DepenseType::class,$depense);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $depense->setEntreprise($entreprise);
                $depense->setUtilisateur($this->getUser());
                $entityManager->persist($depense);
                $entityManager->flush();

                return $this->redirectToRoute('depense_index');
            }
        }

        $model['title'] = 'depense.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/depense/new.html.twig',$model);
    }

    /**
     * @Route("/depense/{id}/edit", name="depense_edit")
     * @param Depense $depense
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Depense $depense, Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DepenseType::class,$depense);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($depense);
                $entityManager->flush();

                return $this->redirectToRoute('depense_index');
            }
        }

        $model['title'] = 'depense.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/depense/edit.html.twig',$model);
    }


    /**
     * @Route("/depense/{id}/delete", name="depense_delete")
     * @param Depense $depense
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Depense $depense, EntityManagerInterface $entityManager): Response
    {
        if (!$depense){
            throw new NotFoundHttpException("Aucune depense avec l'ID ".$depense->getId());
        }
        $entityManager->remove($depense);
        $entityManager->flush();

        return $this->redirectToRoute('depense_index');
    }
}
