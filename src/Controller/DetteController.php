<?php

namespace App\Controller;

use App\Entity\Dette;
use App\Form\DetteType;
use App\Repository\ClientRepository;
use App\Repository\DetteRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class DetteController extends AbstractController
{
    /**
     * @Route("/dette", name="dette_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {

        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise)
            return $this->redirectToRoute('account_logout');

        $model['dettes'] = $entreprise->getDettes();
        $model['title'] = 'dette.index.title';


        return $this->render('admin/dette/index.html.twig', $model);
    }

    /**
     * @Route("/dette/new", name="dette_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param ProduitRepository $produitRepository
     * @param ClientRepository $clientRepository
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        ProduitRepository $produitRepository,
                        ClientRepository $clientRepository,
                        SessionInterface $session,
                        Request $request,
                        EntityManagerInterface $entityManager): Response
    {
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $produits = $produitRepository->findOwnedBy($session->get('entreprise'));
        if (empty($produits)) {
            return $this->redirectToRoute('produit_new');
        }

        $clients = $clientRepository->findOwnedBy($session->get('entreprise'));
        if (empty($clients)) {
            return $this->redirectToRoute('client_new');
        }


        $dette = new Dette();
        $form = $this->createForm(DetteType::class,$dette);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){

                $dette->setEntreprise($entreprise);
                $dette->setUtilisateur($this->getUser());
                $entityManager->persist($dette);
                $entityManager->flush();

                return $this->redirectToRoute('dette_index');
            }
        }

        $model['title'] = 'dette.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/dette/new.html.twig',$model);
    }

    /**
     * @Route("/dette/{id}/edit", name="dette_edit")
     * @param Dette $dette
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Dette $dette, Request $request, EntityManagerInterface $entityManager): Response
    {

        if ($dette->getUtilisateur() !== $this->getUser() &&
            $this->getUser()->getRoles()[0] === 'ROLE_EMPLOYE'){
            $this->addFlash('danger',"flash.dette.not_authorized");
            return $this->redirectToRoute('dette_index');
        }

        $form = $this->createForm(DetteType::class,$dette);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($dette);
                $entityManager->flush();

                return $this->redirectToRoute('dette_index');
            }
        }

        $model['title'] = 'dette.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/dette/edit.html.twig',$model);
    }


    /**
     * @Route("/dette/{id}/delete", name="dette_delete")
     * @param Dette $dette
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Dette $dette, EntityManagerInterface $entityManager): Response
    {
        if (!$dette){
            throw new NotFoundHttpException("Aucune dette avec l'ID ".$dette->getId());
        }

        if ($this->getUser() !== $dette->getUtilisateur() &&
            $this->getUser()->getRoles()[0] === 'ROLE_EMPLOYE'){
            $this->addFlash('danger',"flash.dette.not_authorized");
        }else{
            $entityManager->remove($dette);
            $entityManager->flush();

        }


        return $this->redirectToRoute('dette_index');
    }
}
