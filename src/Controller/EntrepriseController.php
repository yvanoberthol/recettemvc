<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use App\Repository\EntrepriseRepository;
use App\Service\PlatFormEvents;
use App\Service\SouscriptionEvent\SouscriptionEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class EntrepriseController extends AbstractController
{
    /**
     * @Route("/entreprise", name="entreprise_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model['title'] = 'organisation.index.title';


        return $this->render('admin/entreprise/index.html.twig', $model);
    }

    /**
     * @Route("/entreprise/new", name="entreprise_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param EventDispatcherInterface $eventDispatcher
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        SessionInterface $session,
                        EventDispatcherInterface $eventDispatcher,
                        Request $request, EntityManagerInterface $entityManager): Response
    {

        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $entreprise = new Entreprise();
        $form = $this->createForm(EntrepriseType::class,$entreprise);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entreprise->addUtilisateur($this->getUser());
                $entityManager->persist($entreprise);

                $event = new SouscriptionEvent($this->getUser(),$entreprise);
                $eventDispatcher->dispatch($event,PlatFormEvents::CREATE_SOUSCRIPTION_DEFAULT);

                $entityManager->flush();

                return $this->redirectToRoute('entreprise_index');
            }
        }

        $model['title'] = 'organisation.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/entreprise/new.html.twig',$model);
    }

    /**
     * @Route("/entreprise/{id}/edit", name="entreprise_edit")
     * @param Entreprise $entreprise
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Entreprise $entreprise, Request $request, EntityManagerInterface $entityManager): Response
    {

        $form = $this->createForm(EntrepriseType::class,$entreprise);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($entreprise);
                $entityManager->flush();

                return $this->redirectToRoute('entreprise_index');
            }
        }

        $model['title'] = 'organisation.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/entreprise/edit.html.twig',$model);
    }


    /**
     * @Route("/entreprise/{id}/delete", name="entreprise_delete")
     * @param Entreprise $entreprise
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Entreprise $entreprise, EntityManagerInterface $entityManager): Response
    {
        if (!$entreprise){
            throw new NotFoundHttpException("Aucun entreprise avec l'ID ".$entreprise->getId());
        }

        if ($entreprise->getUtilisateurs()->contains($this->getUser()) &&
            $this->getUser()->getRoles()[0] === 'ROLE_ADMIN') {
            $entityManager->remove($entreprise);
            $entityManager->flush();
        }

        return $this->redirectToRoute('entreprise_index');
    }
}
