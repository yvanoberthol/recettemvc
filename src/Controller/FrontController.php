<?php

namespace App\Controller;

use App\Entity\ConfirmTokenMail;
use App\Entity\Entreprise;
use App\Entity\form\RegisterForm;
use App\Entity\PasswordResetToken;
use App\Entity\PasswordUpdate;
use App\Entity\Role;
use App\Entity\Utilisateur;
use App\Form\RegisterType;
use App\Form\ResetPasswordType;
use App\Repository\PackageRepository;
use App\Service\EmailEvent\ConfirmEmailEvent;
use App\Service\PasswordTokenEvent\PasswordResetEvent;
use App\Service\PlatFormEvents;
use App\Service\SouscriptionEvent\SouscriptionEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front_accueil")
     * @param PackageRepository $packageRepository
     * @return Response
     */
    public function index(PackageRepository $packageRepository): Response
    {
        $model['packages'] = $packageRepository->findAll();
        return $this->render('front/index.html.twig',$model);
    }


    /**
     * @Route("/register", name="front_account_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @return RedirectResponse|Response
     */
    public function register(Request $request,
                            UserPasswordEncoderInterface $passwordEncoder,
                            EntityManagerInterface $entityManager,
                            EventDispatcherInterface $eventDispatcher){

        $registerForm = new RegisterForm();
        $roleRepository = $entityManager->getRepository(Role::class);

        $form = $this->createForm(RegisterType::class, $registerForm);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $role = $roleRepository->findOneBy(['name' => 'ROLE_ADMIN']);

            $utilisateur = new Utilisateur();
            $utilisateur->setUsername($registerForm->getUsername());
            $utilisateur->setEmail($registerForm->getEmail());
            $utilisateur->setTelephone($registerForm->getTelephone());
            $utilisateur->setDateNaissance($registerForm->getDateNaissance());
            $encodePassword = $passwordEncoder->encodePassword($utilisateur,$registerForm->getPassword());
            $utilisateur->setPassword($encodePassword);
            $utilisateur->setRole($role);
            $entityManager->persist($utilisateur);

            $organisation = new Entreprise();
            $organisation->setName($registerForm->getEntrepriseName());
            $organisation->addUtilisateur($utilisateur);
            $entityManager->persist($organisation);

            $event = new PasswordResetEvent($utilisateur);
            $eventDispatcher->dispatch($event,PlatFormEvents::CREATE_CONFIRM_MAIL);

            $entityManager->flush();

            $this->addFlash('success', 'flash.front.account_created');
            return $this->redirectToRoute('front_account_register');
        }
        $model['form'] = $form->createView();
        return $this->render('front/account/register.html.twig', $model);
    }

    /**
     * @Route("/confirmationMail", name="confirmation_mail")
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Request $request
     * @return RedirectResponse
     */
    public function confirmMail(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher,
                                Request $request)
    {

        $confirmTokenMailRepository = $entityManager->getRepository(ConfirmTokenMail::class);
        $confirmMailToken = $confirmTokenMailRepository->findOneBy(['token' => $request->get('token')]);


        if ($confirmMailToken === null){
            $this->addFlash('danger','flash.front.invalid_tokenconfirmation');
        }else{
            $utilisateur = $confirmMailToken->getUtilisateur();

            if($utilisateur !== null && $utilisateur->isActif()){
                $this->addFlash('info','flash.front.account_already_valid');
            }else{

                $utilisateur->setActif(true);
                $entityManager->persist($utilisateur);

                // déclenche la création de la souscription par défaut
                $event = new SouscriptionEvent($utilisateur, $utilisateur->getEntreprises()[0]);
                $eventDispatcher->dispatch($event,PlatFormEvents::CREATE_SOUSCRIPTION_DEFAULT);

                $entityManager->flush();
                $this->addFlash('success','flash.front.account_activate');
            }
        }
        return $this->redirectToRoute('account_connexion');
    }


    /**
     * @Route("/form/resendConfirmationMail", name="form_resend_confirmation_mail")
     * @return Response
     */
    public function formResendConfirmatioNmail(): Response
    {
        return $this->render('front/account/resendConfirmationMail.html.twig');
    }
    
    
    /**
     * @Route("/resendConfirmationMail", name="resend_confirmation_mail",methods={"POST"})
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Request $request
     * @return RedirectResponse
     */
    public function resendConfirmMail(EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher,
                                Request $request): RedirectResponse
    {

        $utilisateurRepository = $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $utilisateurRepository->findOneBy(['email' => $request->get('email')]);

        if ($utilisateur === null){
            $this->addFlash('danger','flash.front.email_not_found');
        }elseif ($utilisateur && $utilisateur->isActif()){
            $this->addFlash('danger','flash.front.account_already_activate');
        }elseif ($utilisateur && $utilisateur->getRoles()[0] !== 'ROLE_ADMIN'){
            $this->addFlash('danger','flash.front.not_authorized');
        }else{
            $event = new ConfirmEmailEvent($utilisateur);
            $eventDispatcher->dispatch($event,PlatFormEvents::CREATE_CONFIRM_MAIL);

            $this->addFlash('success','flash.front.send_mailconfirmation');
        }

        return $this->redirectToRoute('form_resend_confirmation_mail');
    }


    /**
     * @Route("/form/sendPasswordTokenMail", name="form_send_passwordtoken_mail")
     * @return Response
     */
    public function formSendPasswordTokenMail(): Response
    {
        return $this->render('front/account/sendPasswordTokenMail.html.twig');
    }

    /**
     * @Route("/sendPasswordTokenMail", name="send_passwordtoken_mail",methods={"POST"})
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Request $request
     * @return RedirectResponse
     */
    public function sendPasswordTokenMail(EntityManagerInterface $entityManager,
                                      EventDispatcherInterface $eventDispatcher,
                                      Request $request): RedirectResponse
    {

        $utilisateurRepository = $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $utilisateurRepository->findOneBy(['email' => $request->get('email')]);

        if ($utilisateur === null){
            $this->addFlash('danger','flash.front.email_not_found');
        }elseif ($utilisateur && $utilisateur->getRoles()[0] !== 'ROLE_ADMIN'){
            $this->addFlash('danger','flash.front.not_authorized');
        }else{
            $event = new PasswordResetEvent($utilisateur);
            $eventDispatcher->dispatch($event,PlatFormEvents::CREATE_LINK_PASSWORD_TOKEN);
            $this->addFlash('success','flash.front.send_linkpassword');
        }

        return $this->redirectToRoute('form_send_passwordtoken_mail');
    }

    /**
     * @Route("/form/resetPassword", name="form_reset_password")
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     */
    public function formResetPassword(EntityManagerInterface $entityManager, Request $request): Response
    {

        $passwordUpdate = new PasswordUpdate();

        $form = $this->createForm(ResetPasswordType::class, $passwordUpdate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $token = $request->get('token');
            if ( $token === null){
                $this->addFlash('danger','flash.front.token_not_found');
                return $this->redirectToRoute('form_reset_password');
            }

            $passwordResetTokenRepository = $entityManager->getRepository(PasswordResetToken::class);
            $passwordResetToken = $passwordResetTokenRepository->findOneBy(['token' => $token]);
            if ($passwordResetToken === null){
                $this->addFlash('danger','flash.front.token_invalid');
                return $this->redirectToRoute('form_reset_password');
            }

            $utilisateur = $passwordResetToken->getUtilisateur();
            if ($utilisateur === null){
                $this->addFlash('danger','flash.front.token_incorrect');
                return $this->redirectToRoute('form_reset_password');
            }

            $utilisateur->setPassword($passwordUpdate->getNewpassword());
            $entityManager->persist($utilisateur);
            $entityManager->flush();
        }

        $model['form'] = $form->createView();
        $token = $request->get('token');
        $model['token'] = $token ?? '';


        return $this->render('front/account/resetPassword.html.twig',$model);
    }
}
