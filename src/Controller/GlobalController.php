<?php


namespace App\Controller;


use App\Repository\EntrepriseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GlobalController extends AbstractController
{

    public function titleEntreprise(EntrepriseRepository $entrepriseRepository,
                                    SessionInterface $session)
    {

        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $model['entreprise'] = $entrepriseRepository->find($session->get('entreprise'));
        if (!$model['entreprise']) {
            return $this->redirectToRoute('account_logout');
        }

        return $this->render('admin/partials/titleEntreprise.html.twig',$model);
    }

}