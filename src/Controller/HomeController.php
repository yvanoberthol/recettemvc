<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Repository\EntrepriseRepository;
use App\Repository\PackageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class HomeController extends AbstractController
{

    /**
     * @Route("/statRecette", name="recette_stat")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function statRecette(EntrepriseRepository $entrepriseRepository,
                                SessionInterface $session, Request $request): Response
    {

        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model = $this->getProduitStat($entreprise,$request->get('id'));
        if ($model['produits'] === []) {
            return $this->redirectToRoute('produit_new');
        }

        $model['title'] = 'statrecette.title';
        return $this->render('admin/statistique/recette.html.twig',$model);
    }

    /**
     * @Route("/statDepense", name="depense_stat")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function statDepense(EntrepriseRepository $entrepriseRepository,
                                SessionInterface $session, Request $request): Response
    {

        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model = $this->getProduitStat($entreprise,$request->get('id'));
        if ($model['produits'] === []) {
            return $this->redirectToRoute('produit_new');
        }

        $model['title'] = 'statdepense.title';
        return $this->render('admin/statistique/depense.html.twig',$model);
    }

    /**
     * @Route({"/"}, name="accueil")
     * @param EntrepriseRepository $entrepriseRepository
     * @param PackageRepository $packageRepository
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          PackageRepository $packageRepository,
                          SessionInterface $session, Request $request): Response
    {
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));


        if ($entreprise === null) {
            return $this->redirectToRoute('account_logout');
        }

        $model = $this->getProduitGeneral($entreprise, $request->get('id'));


        $model['entreprise'] = $entreprise;

        $model['title'] = 'accueil.title';
        $model['utilisateur']  = $this->getUser();
        $model['packages'] = $packageRepository->findAll();

        return $this->render('admin/accueil.html.twig',$model);
    }

    private function getProduitGeneral(Entreprise $entreprise, $id){
        $model = $this->getProduits($entreprise, $id);
        $model['utilisateurs']  = $entreprise->getUtilisateurs();
        $year = date('Y');
        $model['annees'] = [
            $year,
            $year - 1,
            $year - 2,
            $year - 3,
            $year - 4
        ];

        return $model;
    }

    private function getProduitStat(Entreprise $entreprise, $id){

        $model = $this->getProduits($entreprise, $id);
        if ($model === null) {
            return null;
        }

        $model['months'] = [
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août',
            9 => 'Septembre',
            10 => 'Octobre',
            11 => 'Novembre',
            12 => 'Décembre'
        ];
        $model['monthNow'] = date('n');

        $year = date('Y');
        $model['annees'] = [
            $year,
            $year - 1,
            $year - 2,
            $year - 3,
            $year - 4
        ];

        return $model;
    }

    public function getProduits(Entreprise $entreprise, $id)
    {
        if ($entreprise === null){
            return null;
        }

        $model['produits'] = $entreprise->getProduits();
        $model['produit'] = null;
        if ($entreprise->getProduits()->isEmpty()) {
            $model['produits'] = [];
        }

        if ($model['produits'] !== []){
            if ($id === null) {
                $id = $model['produits'][0]->getId();
            }
            $model['produit']  =
                $model['produits']->filter(static function ($produit) use($id){
                return $produit->getId() === (int) $id;
            })->first();
        }

        return $model;
    }
}
