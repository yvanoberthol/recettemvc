<?php

namespace App\Controller;

use App\Repository\ParametreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 */
class ParametreController extends AbstractController
{
    /**
     * @Route("/parametre", name="parametre")
     * @param ParametreRepository $parametreRepository
     * @return Response
     */
    public function index(ParametreRepository $parametreRepository)
    {
        $model['title'] = 'parametre.title';
        $model['parametres'] = $parametreRepository->findAll();
        return $this->render('admin/parametre/index.html.twig', $model);
    }

    /**
     * @Route("/parametre/update", name="parametre_update", methods={"POST"})
     * @param Request $request
     * @param ParametreRepository $parametreRepository
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse
     */
    public function update(Request $request, ParametreRepository $parametreRepository,
                           EntityManagerInterface $entityManager): RedirectResponse
    {
        foreach ($request->get('code') as $code) {
            $parametre = $parametreRepository->findOneBy(['code' => $code]);
            if ($parametre){
                $parametre->setValue($request->get('value')[$parametre->getId()]);
                $entityManager->persist($parametre);
            }
        }

        $entityManager->flush();
        return $this->redirectToRoute('parametre');
    }
}
