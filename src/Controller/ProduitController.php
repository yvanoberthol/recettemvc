<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\EntrepriseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class ProduitController extends AbstractController
{

    /**
     * @Route("/produit", name="produit_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model['produits'] = $entreprise->getProduits();
        $model['title'] = 'produit.index.title';


        return $this->render('admin/produit/index.html.twig', $model);
    }

    /**
     * @Route("/produit/new", name="produit_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        SessionInterface $session,
                        Request $request, EntityManagerInterface $entityManager): Response
    {

        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $produit = new Produit();
        $form = $this->createForm(ProduitType::class,$produit);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $produit->setEntreprise($entreprise);

                $entityManager->persist($produit);
                $entityManager->flush();

                return $this->redirectToRoute('produit_index');
            }
        }

        $model['title'] = 'produit.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/produit/new.html.twig',$model);
    }

    /**
     * @Route("/produit/{id}/edit", name="produit_edit")
     * @param Produit $produit
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Produit $produit, Request $request, EntityManagerInterface $entityManager): Response
    {

        $form = $this->createForm(ProduitType::class,$produit);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($produit);
                $entityManager->flush();

                return $this->redirectToRoute('produit_index');
            }
        }

        $model['title'] = 'produit.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/produit/edit.html.twig',$model);
    }


    /**
     * @Route("/produit/{id}/delete", name="produit_delete")
     * @param Produit $produit
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Produit $produit, EntityManagerInterface $entityManager): Response
    {
        if (!$produit){
            throw new NotFoundHttpException("Aucun produit avec l'ID ".$produit->getId());
        }
        $entityManager->remove($produit);
        $entityManager->flush();

        return $this->redirectToRoute('produit_index');
    }
}
