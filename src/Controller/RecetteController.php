<?php

namespace App\Controller;

use App\Entity\Recette;
use App\Form\RecetteType;
use App\Repository\EntrepriseRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class RecetteController extends AbstractController
{
    /**
     * @Route("/recette", name="recette_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model['recettes'] = $entreprise->getRecettes();
        $model['title'] = 'recette.index.title';


        return $this->render('admin/recette/index.html.twig', $model);
    }

    /**
     * @Route("/recette/new", name="recette_new")
     * @param EntrepriseRepository $entrepriseRepository
     * @param ProduitRepository $produitRepository
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(EntrepriseRepository $entrepriseRepository,
                        ProduitRepository $produitRepository,
                        SessionInterface $session,
                        Request $request, EntityManagerInterface $entityManager): Response
    {
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $produits = $produitRepository->findOwnedBy($session->get('entreprise'));
        if (empty($produits)) {
            return $this->redirectToRoute('produit_new');
        }



        $recette = new Recette();
        $form = $this->createForm(RecetteType::class,$recette);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){

                $recette->setEntreprise($entreprise);
                $recette->setUtilisateur($this->getUser());
                $entityManager->persist($recette);
                $entityManager->flush();

                return $this->redirectToRoute('recette_index');
            }
        }

        $model['title'] = 'recette.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/recette/new.html.twig',$model);
    }

    /**
     * @Route("/recette/{id}/edit", name="recette_edit")
     * @param Recette $recette
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Recette $recette, Request $request, EntityManagerInterface $entityManager): Response
    {

        if ($recette->getUtilisateur() !== $this->getUser() &&
            $this->getUser()->getRoles()[0] === 'ROLE_EMPLOYE'){
            $this->addFlash('danger',"flash.recette.not_authorized");
            return $this->redirectToRoute('recette_index');
        }

        $form = $this->createForm(RecetteType::class,$recette);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($recette);
                $entityManager->flush();

                return $this->redirectToRoute('recette_index');
            }
        }

        $model['title'] = 'recette.edit.title';
        $model['form'] = $form->createView();
        return $this->render('admin/recette/edit.html.twig',$model);
    }


    /**
     * @Route("/recette/{id}/delete", name="recette_delete")
     * @param Recette $recette
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Recette $recette, EntityManagerInterface $entityManager): Response
    {
        if (!$recette){
            throw new NotFoundHttpException("Aucune recette avec l'ID ".$recette->getId());
        }
        if ($this->getUser() === $recette->getUtilisateur() ||
            $this->getUser()->getRoles()[0] !== 'ROLE_EMPLOYE'){
            $entityManager->remove($recette);
            $entityManager->flush();
        }else{
            $this->addFlash('danger',"flash.recette.not_authorized");
        }

        return $this->redirectToRoute('recette_index');
    }
}
