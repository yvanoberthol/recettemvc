<?php

namespace App\Controller;

use App\Entity\Dette;
use App\Entity\Reglement;
use App\Form\ReglementType;
use App\Repository\EntrepriseRepository;
use App\Repository\ReglementRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class ReglementController extends AbstractController
{
    /**
     * @Route("/reglement", name="reglement_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @param ReglementRepository $reglementRepository
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session,
                          ReglementRepository $reglementRepository): Response
    {

        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model['reglements'] = $reglementRepository->findByEntreprise($entreprise);
        $model['title'] = 'reglement.index.title';


        return $this->render('admin/reglement/index.html.twig', $model);
    }
    
    /**
     * @Route("/reglement/new", name="reglement_new",methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse
     * @throws Exception
     */
    public function new(Request $request, EntityManagerInterface $entityManager): RedirectResponse
    {
        $detteRepository = $entityManager->getRepository(Dette::class);
        $dette = $detteRepository->find($request->get('dette_id'));

        if (!$dette){
            throw new NotFoundHttpException("Aucune dette avec l'ID ".$dette->getId());
        }

        $date =  new DateTime($request->get('date'));

        if ($date > $dette->getDate()){
            if ($date <= new DateTime()) {
                if ($dette->getReste() >= $request->get('montant')) {
                    $reglement = new Reglement();
                    $reglement->setDate($date);
                    $reglement->setMontant($request->get('montant'));
                    $reglement->setDette($dette);
                    $reglement->setUtilisateur($this->getUser());

                    $entityManager->persist($reglement);
                    $entityManager->flush();

                    $this->addFlash('success', "flash.reglement.added");
                } else {
                    $this->addFlash('danger', "flash.reglement.amount_is_up");
                }
            } else {
                $this->addFlash('danger', "flash.reglement.date_is_up");
            }

        }else{
            $this->addFlash('danger', "flash.reglement.date_is_down");
        }

        return $this->redirectToRoute('dette_index');
    }

    /**
     * @Route("/reglement/{id}/edit", name="reglement_edit")
     * @param Reglement $reglement
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Reglement $reglement, Request $request, EntityManagerInterface $entityManager): Response
    {
        if ($reglement->getUtilisateur() !== $this->getUser() &&
            $this->getUser()->getRoles()[0] === 'ROLE_EMPLOYE'){
            $this->addFlash('danger',"flash.reglement.not_authorized");
            return $this->redirectToRoute('dette_index');
        }

        $form = $this->createForm(ReglementType::class, $reglement);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){
                $entityManager->persist($reglement);
                $entityManager->flush();

                return $this->redirectToRoute('dette_index');
            }
        }

        $model['title'] = 'reglement.edit.title';
        $model['form'] = $form->createView();

        return $this->render('admin/reglement/edit.html.twig', $model);
    }

    /**
     * @Route("/reglement/{id}/delete", name="reglement_delete")
     * @param Reglement $reglement
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function delete(Reglement $reglement, EntityManagerInterface $entityManager): Response
    {
        if (!$reglement){
            throw new NotFoundHttpException("Aucun règlement avec l'ID ".$reglement->getId());
        }
        if ($this->getUser() !== $reglement->getUtilisateur() &&
            $this->getUser()->getRoles()[0] === 'ROLE_EMPLOYE'){
            $this->addFlash('danger',"flash.reglement.not_authorized");
        }else{
            $entityManager->remove($reglement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dette_index');
    }


}
