<?php


namespace App\Controller;



use App\Entity\Entreprise;
use App\Entity\Package;
use App\Entity\Souscription;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 */
class SouscriptionController extends AbstractController
{

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/souscription/renew",name="souscription_renew",methods={"POST"})
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse
     * @throws Exception
     */
    public function renew(Request $request,
                          SessionInterface $session,
                          EntityManagerInterface $entityManager): RedirectResponse
    {

        $packageRepository = $entityManager->getRepository(Package::class);
        $entrepriseRepository = $entityManager->getRepository(Entreprise::class);
        $souscriptionRepository = $entityManager->getRepository(Souscription::class);

        $package = $packageRepository->find($request->get('package_id'));
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        $souscriptionUpdate = $souscriptionRepository->find($request->get('souscription_id'));

        if ($package === null || $entreprise === null || $souscriptionUpdate === null) {
            return $this->redirectToRoute('accueil');
        }


        if (!empty($utilisateur = $this->getUser())){

            // verify admin balance
            if ($utilisateur->getBalance() < $package->getPrix()){
                $this->addFlash('danger', "flash.souscription.balance_down");
                return $this->redirectToRoute('accueil');
            }

            // recover the balace rest to update the balance admin
            $soldeRestant = $utilisateur->getBalance() - $package->getPrix();
            $utilisateur->setBalance($soldeRestant);

            // recover the update date
            $dateUpdate = ($souscriptionUpdate->getExpiration() > new DateTime())? $souscriptionUpdate->getExpiration() : new DateTime();

            $souscriptionUpdate->setDate($dateUpdate);
            $souscriptionUpdate->setPackage($package);

            // persist user and subscription
            $entityManager->persist($souscriptionUpdate);
            $entityManager->persist($utilisateur);
            $entityManager->flush();

            $this->addFlash('success', 'flash.souscription.renew');
        }

        return $this->redirectToRoute('accueil');

    }

}