<?php


namespace App\Controller;


use App\Entity\Utilisateur;
use App\Form\GoogleAuthenticatorType;
use App\Suscriber\TwoFactorAuthenticationSubscriber;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Doctrine\ORM\EntityManagerInterface;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FA\Google2FA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

/**
 * @Route("/admin")
 */
class TwoFactorAuthenticationController extends AbstractController
{
    /**
     * @Route("/two_factor", name="account_two_factor")
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     */
    public function twoFactorAction(Request $request, TokenStorageInterface $tokenStorage,
                                    SessionInterface $session,
                                    EntityManagerInterface $entityManager)
    {

        if (!$this->getUser()->isGoogleauthenticatorenable()){
            return $this->redirectToRoute('accueil');
        }

        $form = $this->createForm(GoogleAuthenticatorType::class);

        $form->handleRequest($request);
        $google2fa = new Google2FA();

        $svg = null;

        /** @var Utilisateur $utilisateur */
        $utilisateur = $this->getUser();
        if (!$utilisateur->getGoogleAuthenticatorSecret()) {
            if ($session->get('2fa_secret')) {
                $secret = $session->get('2fa_secret');
            } else {
                $secret = $google2fa->generateSecretKey();
                $request->getSession()->set('2fa_secret', $secret);
            }

            $svg = $this->generateSvgForUser($google2fa, $utilisateur, $secret);
        } else {
            $secret = $utilisateur->getGoogleAuthenticatorSecret();
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $code = $form->getData()["code"];
            $codeIsValid = $google2fa->verifyKey($secret, $code, 4);
            if ($codeIsValid) {
                if (!$utilisateur->getGoogleAuthenticatorSecret()) {
                    $utilisateur->setGoogleAuthenticatorSecret($secret);
                    $entityManager->persist($utilisateur);
                    $entityManager->flush();
                }

                $this->addRoleTwoFA($tokenStorage, $session);

                return $this->redirectToRoute("accueil");
            }
            $this->addFlash("danger", "flash.two_factor.code_invalid");
        }

        return $this->render("admin/account/two-factor.html.twig", [
            "svg" => $svg,
            "form" => $form->createView(),
        ]);
    }

    private function generateSvgForUser(Google2FA $google2FA, Utilisateur $utilisateur, string $secret): string
    {
        $g2faUrl = $google2FA->getQRCodeUrl(
            "Recetta",
            $utilisateur->getUsername(),
            $secret
        );

        $writer = new Writer(
            new ImageRenderer(
                new RendererStyle(400),
                new SvgImageBackEnd() // can also user new ImagickImageBackEnd() in order to generate png
            )
        );

        return $writer->writeString($g2faUrl);
    }

    private function addRoleTwoFA(TokenStorageInterface $tokenStorage, SessionInterface $session): void
    {
        /** @var PostAuthenticationGuardToken $currentToken */
        $currentToken = $tokenStorage->getToken();
        $roles = array_merge($currentToken->getRoleNames(), [TwoFactorAuthenticationSubscriber::ROLE_2FA_SUCCEED]);
        $newToken = new PostAuthenticationGuardToken($currentToken->getUser(), $currentToken->getProviderKey(), $roles);
        $tokenStorage->setToken($newToken);
        $session->set('_security_' . $currentToken->getProviderKey(), serialize($newToken));
    }
}