<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Utilisateur;
use App\Form\UtilisateurRegisterType;
use App\Repository\EntrepriseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/utilisateur", name="utilisateur_index")
     * @param EntrepriseRepository $entrepriseRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function index(EntrepriseRepository $entrepriseRepository,
                          SessionInterface $session): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));
        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $model['utilisateurs'] = $entreprise->getUtilisateurs();
        $model['title'] = 'utilisateur.index.title';


        return $this->render('admin/utilisateur/index.html.twig', $model);
    }


    /**
     * @Route("/utilisateur/new", name="utilisateur_new")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder,
                        SessionInterface $session,EntityManagerInterface $entityManager): Response
    {
        //recupérer et vérifier l'existence de l'entreprise récupérée à la connexion
        $entrepriseRepository = $entityManager->getRepository(Entreprise::class);
        $entreprise = $entrepriseRepository->find($session->get('entreprise'));

        if (!$entreprise) {
            return $this->redirectToRoute('account_logout');
        }

        $utilisateur = new Utilisateur();

        $form = $this->createForm(UtilisateurRegisterType::class,$utilisateur);

        if ($request->isMethod("POST")){
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()){

                $encodePassword = $passwordEncoder->encodePassword($utilisateur,$utilisateur->getPassword());
                $utilisateur->setPassword($encodePassword);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                return $this->redirectToRoute('utilisateur_index');
            }
        }

        $model['title'] = 'utilisateur.new.title';
        $model['form'] = $form->createView();
        return $this->render('admin/utilisateur/new.html.twig',$model);
    }


    /**
     * @Route("/utilisateur/etat", name="utilisateur_etat",methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function etat(Request $request, EntityManagerInterface $entityManager): Response
    {
        $utilisateurRepository = $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $utilisateurRepository->find($request->get('user_id'));

        if ($utilisateur === null){
            throw new NotFoundHttpException("l'utilisateur n'existe pas");
        }

        $utilisateur->setActif($request->get('actif'));
        $entityManager->persist($utilisateur);
        $entityManager->flush();

        return $this->redirectToRoute('utilisateur_index');
    }
}
