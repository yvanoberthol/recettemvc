<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(min="4",minMessage="Entrez un nom compréhensif")
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Dette::class, mappedBy="client")
     */
    private $dettes;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="clients")
     * @ORM\JoinColumn(nullable=false)
     * @var Entreprise
     */
    private $entreprise;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Client
     */
    public function setName(string $name): Client
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|Dette[]
     */
    public function getDettes(): Collection
    {
        return $this->dettes;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Client
     */
    public function setEntreprise(Entreprise $entreprise): Client
    {
        $this->entreprise = $entreprise;
        return $this;
    }
}
