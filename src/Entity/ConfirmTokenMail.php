<?php

namespace App\Entity;

use App\Repository\ConfirmTokenMailRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ConfirmTokenMailRepository::class)
 */
class ConfirmTokenMail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var float|int
     */
    private $daysExpiration = 2 * 24 * 3600; // 2jours

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $token;

    /**
     * @ORM\OneToOne(targetEntity=Utilisateur::class,
     *     inversedBy="confirmTokenMail", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return ConfirmTokenMail
     */
    public function setCreatedAt(DateTime $createdAt): ConfirmTokenMail
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return float|int
     */
    public function getDaysExpiration()
    {
        return $this->daysExpiration;
    }

    /**
     * @param float|int $daysExpiration
     * @return ConfirmTokenMail
     */
    public function setDaysExpiration($daysExpiration)
    {
        $this->daysExpiration = $daysExpiration;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ConfirmTokenMail
     */
    public function setToken(string $token): ConfirmTokenMail
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return ConfirmTokenMail
     */
    public function setUtilisateur(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    public function getExpiration(): ?int
    {
        return $this->createdAt->getTimeStamp() + $this->daysExpiration;
    }

}
