<?php

namespace App\Entity;

use App\Repository\DepenseRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DepenseRepository::class)
 */
class Depense
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Assert\Type("\DateTimeInterface",message="Entrez une date valide")
     * @Assert\LessThanOrEqual("today",message="La date ne doit pas dépasser celle d'aujourd'hui")
     * @var DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Entrez l'objet de la dépense")
     * @var string
     */
    private $raison;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="Entrez un montant positif")
     * @var integer
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="depenses")
     * @ORM\JoinColumn(nullable=false)
     * @var Produit
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="depenses")
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="depenses")
     * @ORM\JoinColumn(nullable=false)
     * @var Entreprise
     */
    private $entreprise;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Depense
     */
    public function setDate(DateTime $date): Depense
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getRaison(): ?string
    {
        return $this->raison;
    }

    /**
     * @param string $raison
     * @return Depense
     */
    public function setRaison(string $raison): Depense
    {
        $this->raison = $raison;
        return $this;
    }

    /**
     * @return int
     */
    public function getMontant(): ?int
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     * @return Depense
     */
    public function setMontant(int $montant): Depense
    {
        $this->montant = $montant;
        return $this;
    }

    /**
     * @return Produit
     */
    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    /**
     * @param Produit $produit
     * @return Depense
     */
    public function setProduit(Produit $produit): Depense
    {
        $this->produit = $produit;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Depense
     */
    public function setUtilisateur(Utilisateur $utilisateur): Depense
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Depense
     */
    public function setEntreprise(Entreprise $entreprise): Depense
    {
        $this->entreprise = $entreprise;
        return $this;
    }

}
