<?php

namespace App\Entity;

use App\Repository\DetteRepository;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DetteRepository::class)
 */
class Dette
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Assert\Type("\DateTimeInterface",message="Entrez une date valide")
     * @Assert\LessThanOrEqual("today",message="La date ne doit pas dépasser celle d'aujourd'hui")
     * @var DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="Entrez un montant positif")
     * @var integer
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="dettes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="dettes")
     * @ORM\JoinColumn(nullable=false)
     * @var Client
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="dettes")
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="dette",orphanRemoval=true)
     */
    private $reglements;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="dettes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entreprise;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection | Reglement[]
     */
    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Dette
     */
    public function setDate(DateTime $date): Dette
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getMontant(): ?int
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     * @return Dette
     */
    public function setMontant(int $montant): Dette
    {
        $this->montant = $montant;
        return $this;
    }

    /**
     * @return Produit
     */
    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    /**
     * @param Produit $produit
     * @return Dette
     */
    public function setProduit(Produit $produit): Dette
    {
        $this->produit = $produit;
        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return Dette
     */
    public function setClient(Client $client): Dette
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Dette
     */
    public function setUtilisateur(Utilisateur $utilisateur): Dette
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Dette
     */
    public function setEntreprise(Entreprise $entreprise): Dette
    {
        $this->entreprise = $entreprise;
        return $this;
    }


    public function getMontantReglee(): ?int
    {
       return array_reduce($this->reglements->toArray(), static function ($total, $reglement){
            return $total + $reglement->getMontant();
        }, 0);
    }

    public function isReglee(): ?bool
    {
        return $this->montant === $this->getMontantReglee();
    }

    public function getReste(): ?int
    {
        return $this->montant - $this->getMontantReglee();
    }
}
