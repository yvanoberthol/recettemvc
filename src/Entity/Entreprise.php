<?php

namespace App\Entity;

use App\Repository\EntrepriseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository", repositoryClass=EntrepriseRepository::class)
 * @UniqueEntity(
 *     fields={"name"},
 *     message="une organisation existe déjà sur ce nom"
 * )
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $matricule;

    /**
     * @ORM\ManyToMany(targetEntity=Utilisateur::class, mappedBy="entreprises")
     */
    private $utilisateurs;

    /**
     * @ORM\OneToOne(targetEntity=Souscription::class, mappedBy="entreprise")
     * @var Souscription
     */
    private $souscription;

    /**
     * @ORM\OneToMany(targetEntity=Depense::class, mappedBy="entreprise")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $depenses;

    /**
     * @ORM\OneToMany(targetEntity=Recette::class, mappedBy="entreprise")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $recettes;

    /**
     * @ORM\OneToMany(targetEntity=Dette::class, mappedBy="entreprise")
     */
    private $dettes;

    /**
     * @ORM\OneToMany(targetEntity=Produit::class, mappedBy="entreprise")
     */
    private $produits;

    /**
     * @ORM\OneToMany(targetEntity=Client::class, mappedBy="entreprise")
     */
    private $clients;

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
      $this->utilisateurs  = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Entreprise
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    /**
     * @param string $matricule
     * @return Entreprise
     */
    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;
        return $this;
    }

    /**
     * @return Souscription
     */
    public function getSouscription(): ?Souscription
    {
        return $this->souscription;
    }

    /**
     * @param Souscription $souscription
     * @return Entreprise
     */
    public function setSouscription(Souscription $souscription): self
    {
        $this->souscription = $souscription;
        return $this;
    }

    /**
     * @return Collection|Depense[]
     */
    public function getDepenses(): Collection
    {
        return $this->depenses;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }


    /**
     * @return Collection|Recette[]
     */
    public function getRecettes(): Collection
    {
        return $this->recettes;
    }

    /**
     * @return Collection|Dette[]
     */
    public function getDettes(): Collection
    {
        return $this->dettes;
    }


    /**
     * @return Collection|Produit[]
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }


    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)){
            $this->utilisateurs[] = $utilisateur;
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->contains($utilisateur)){
            $this->utilisateurs->remove($utilisateur);
        }

        return $this;
    }

}