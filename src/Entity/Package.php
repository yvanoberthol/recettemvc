<?php

namespace App\Entity;

use App\Repository\PackageRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=PackageRepository::class)
 * @UniqueEntity(
 *     fields={"name"},
 *     message="ce package existe déjà"
 * )
 */
class Package
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $prix;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $tauxReduction = 0;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $nbreJours;

    /**
     * @ORM\OneToMany(targetEntity=Souscription::class, mappedBy="package")
     */
    private $souscriptions;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection| Souscription[]
     */
    public function getSouscriptions(): Collection
    {
        return $this->souscriptions;
    }

    public function getPrix(): ?int
    {
        return $this->prix + ($this->prix * $this->tauxReduction/100);
    }

    /**
     * @param int $prix
     * @return Package
     */
    public function setPrix(int $prix): Package
    {
        $this->prix = $prix;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Package
     */
    public function setName(string $name): Package
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getTauxReduction(): ?float
    {
        return $this->tauxReduction;
    }

    /**
     * @param float $tauxReduction
     * @return Package
     */
    public function setTauxReduction(float $tauxReduction): Package
    {
        $this->tauxReduction = $tauxReduction;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbreJours(): ?int
    {
        return $this->nbreJours;
    }

    /**
     * @param int $nbreJours
     * @return Package
     */
    public function setNbreJours(int $nbreJours): Package
    {
        $this->nbreJours = $nbreJours;
        return $this;
    }

}
