<?php

namespace App\Entity;

use App\Repository\PasswordResetTokenRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PasswordResetTokenRepository::class)
 */
class PasswordResetToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var float|int
     */
    private $daysExpiration = 2 * 24 * 3600; // 2jours

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $token;

    /**
     * @ORM\OneToOne(targetEntity=Utilisateur::class, inversedBy="passwordResetToken", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return PasswordResetToken
     */
    public function setCreatedAt(DateTime $createdAt): PasswordResetToken
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return PasswordResetToken
     */
    public function setToken(string $token): PasswordResetToken
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return PasswordResetToken
     */
    public function setUtilisateur(Utilisateur $utilisateur): PasswordResetToken
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    public function getExpiration(): ?int
    {
        return $this->createdAt->getTimeStamp() + $this->daysExpiration;
    }
}
