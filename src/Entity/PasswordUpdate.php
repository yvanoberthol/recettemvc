<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PasswordUpdate
 * @package App\Entity
 */
class PasswordUpdate
{

    /**
     * @var string
     */
    private $oldpassword;

    /**
     * @var string
     * @Assert\Length(min=6,minMessage="Le mot de passe doit contenir au moins 6 caractères")
     */
    private $newpassword;

    /**
     * @var string
     * @Assert\EqualTo(propertyPath="newpassword",message="Mot de passe de confirmation incorrect")
     */
    private $confirmpassword;

    /**
     * @return string
     */
    public function getOldpassword(): ?string
    {
        return $this->oldpassword;
    }

    /**
     * @param string $oldpassword
     * @return PasswordUpdate
     */
    public function setOldpassword(string $oldpassword): PasswordUpdate
    {
        $this->oldpassword = $oldpassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewpassword(): ?string
    {
        return $this->newpassword;
    }

    /**
     * @param string $newpassword
     * @return PasswordUpdate
     */
    public function setNewpassword(string $newpassword): PasswordUpdate
    {
        $this->newpassword = $newpassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmpassword(): ?string
    {
        return $this->confirmpassword;
    }

    /**
     * @param string $confirmpassword
     * @return PasswordUpdate
     */
    public function setConfirmpassword(string $confirmpassword): PasswordUpdate
    {
        $this->confirmpassword = $confirmpassword;
        return $this;
    }
}
