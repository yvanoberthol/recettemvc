<?php

namespace App\Entity;

use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProduitRepository::class)
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=75)
     * @Assert\NotBlank(message="Entrez un nom de produit")
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Recette::class, mappedBy="produit")
     * @ORM\OrderBy({"date" = "ASC"})[]
     */
    private $recettes;

    /**
     * @ORM\OneToMany(targetEntity=Depense::class, mappedBy="produit")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $depenses;

    /**
     * @ORM\OneToMany(targetEntity=Dette::class, mappedBy="produit")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $dettes;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     * @var Entreprise
     */
    private $entreprise;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Produit
     */
    public function setName(string $name): Produit
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Produit
     */
    public function setEntreprise(Entreprise $entreprise): Produit
    {
        $this->entreprise = $entreprise;
        return $this;
    }

    /**
     * @return Collection|Recette[]
     */
    public function getRecettes(): Collection
    {
        return $this->recettes;
    }

    /**
     * @return Collection|Depense[]
     */
    public function getDepenses(): Collection
    {
        return $this->depenses;
    }

    /**
     * @return Collection|Dette[]
     */
    public function getDettes(): Collection
    {
        return $this->dettes;
    }

    public function getRecetteTotal(): ?int
    {
        return array_reduce($this->recettes->toArray(), static function ($total,$recette){
            return $total + $recette->getMontant();
        },0);

    }

    public function getDepenseTotal(): ?int
    {
        return array_reduce($this->depenses->toArray(), static function ($total,$depense){
            return $total + $depense->getMontant();
        },0);
    }

    public function getDetteTotalPrise(): ?int
    {
        return array_reduce($this->dettes->toArray(), static function ($total,$dette){
            return $total + $dette->getMontant();
        },0);
    }

    public function getDetteMontantRegle(): ?int
    {
        return array_reduce($this->dettes->toArray(), static function ($total,$dette){
            return $total + $dette->getMontantReglee();
        },0);
    }

    public function getDetteReste(): ?int
    {
        return array_reduce($this->dettes->toArray(), static function ($total,$dette){
            return $total + $dette->getReste();
        },0);
    }

    public function getBenefice(): ?int
    {
        return ($this->getRecetteTotal() + $this->getDetteMontantRegle()) - $this->getDepenseTotal();
    }
}
