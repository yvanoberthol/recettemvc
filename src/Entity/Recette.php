<?php

namespace App\Entity;

use App\Repository\RecetteRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RecetteRepository::class)
 */
class Recette
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="Entrez un montant positif")
     * @var integer
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @ORM\Column(type="date")
     * @Assert\Type("\DateTimeInterface",message="Entrez une date valide")
     * @Assert\LessThanOrEqual("today",message="La date ne doit pas dépasser celle d'aujourd'hui")
     * @var DateTime
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     * @var Produit
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     * @var Entreprise
     */
    private $entreprise;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMontant(): ?int
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     * @return Recette
     */
    public function setMontant(int $montant): Recette
    {
        $this->montant = $montant;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Recette
     */
    public function setUtilisateur(Utilisateur $utilisateur): Recette
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Recette
     */
    public function setDate(DateTime $date): Recette
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Produit
     */
    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    /**
     * @param Produit $produit
     * @return Recette
     */
    public function setProduit(Produit $produit): ?Recette
    {
        $this->produit = $produit;
        return $this;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Recette
     */
    public function setEntreprise(Entreprise $entreprise): Recette
    {
        $this->entreprise = $entreprise;
        return $this;
    }
}
