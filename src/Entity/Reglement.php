<?php

namespace App\Entity;

use App\Repository\ReglementRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReglementRepository::class)
 */
class Reglement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Assert\Type("\DateTimeInterface",message="Entrez une date valide")
     * @Assert\LessThanOrEqual("today",message="La date ne doit pas dépasser celle d'aujourd'hui")
     * @var DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="Entrez un montant positif")
     * @var integer
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=Dette::class, inversedBy="reglements")
     * @ORM\JoinColumn(nullable=false)
     * @var Dette
     */
    private $dette;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="reglements")
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Reglement
     */
    public function setDate(DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getMontant(): ?int
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     * @return Reglement
     */
    public function setMontant(int $montant): self
    {
        $this->montant = $montant;
        return $this;
    }

    /**
     * @return Dette
     */
    public function getDette(): Dette
    {
        return $this->dette;
    }

    /**
     * @param Dette $dette
     * @return Reglement
     */
    public function setDette(Dette $dette): self
    {
        $this->dette = $dette;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Reglement
     */
    public function setUtilisateur(Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }
}
