<?php


namespace App\Entity;

use App\Repository\SouscriptionRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=SouscriptionRepository::class)
 */
class Souscription
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="souscriptions")
     * @ORM\JoinColumn(nullable=false)
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity=Package::class, inversedBy="souscriptions")
     * @ORM\JoinColumn(nullable=false)
     * @var Package
     */
    private $package;

    /**
     * @ORM\OneToOne(targetEntity=Entreprise::class, inversedBy="souscription")
     * @ORM\JoinColumn(nullable=false)
     * @var Entreprise
     */
    private $entreprise;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $created_at;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Souscription
     */
    public function setUtilisateur(Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    /**
     * @return Package
     */
    public function getPackage(): ?Package
    {
        return $this->package;
    }

    /**
     * @param Package $package
     * @return Souscription
     */
    public function setPackage(Package $package): self
    {
        $this->package = $package;
        return $this;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    /**
     * @param Entreprise $entreprise
     * @return Souscription
     */
    public function setEntreprise(Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Souscription
     */
    public function setDate(DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime $created_at
     * @return Souscription
     */
    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getExpiration(): ?DateTime
    {
        $timestampJourExpiration = $this->package->getNbreJours() * 24 * 3600;
        $dateTimeStamp = $this->date->getTimeStamp();

        $expirationTimeStamp = $timestampJourExpiration + $dateTimeStamp;

        return new DateTime(date('Y-m-d', $expirationTimeStamp));
    }

    public function getNbreJourRestants(): ?int
    {
        $today = new DateTime();
        $dateToday = date_create($today->format('Y-m-d'));
        $expirationDate = date_create($this->getExpiration()->format('Y-m-d'));

        return $this->expirationisUpThanToday()? $expirationDate->diff($dateToday)->days: 0;
    }

    public function expirationisUpThanToday(): ?bool
    {
        return $this->getExpiration() > new DateTime();
    }

}