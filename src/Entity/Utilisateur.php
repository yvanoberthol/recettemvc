<?php

namespace App\Entity;

use App\Repository\UtilisateurRepository;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 * @UniqueEntity(
 *     fields={"username","email","telephone"},
 *     message="un utilisateur existe déjà avec cette information dans une organisation"
 * )
 */
class Utilisateur implements UserInterface, EquatableInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $telephone;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $dateNaissance;

    /**
     * @ORM\ManyToOne(targetEntity=Role::class)
     * @ORM\JoinColumn(nullable=false)
     * @var Role
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity=Recette::class, mappedBy="utilisateur")
     */
    private $recettes;

    /**
     * @ORM\OneToMany(targetEntity=Depense::class, mappedBy="utilisateur")
     */
    private $depenses;

    /**
     * @ORM\OneToMany(targetEntity=Dette::class, mappedBy="utilisateur")
     */
    private $dettes;

    /**
     * @ORM\OneToMany(targetEntity=Souscription::class, mappedBy="utilisateur")
     */
    private $souscriptions;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $actif = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $lastConnexion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $lastDeconnexion;

    /**
     * @ORM\ManyToMany(targetEntity=Entreprise::class, inversedBy="utilisateurs")
     */
    private $entreprises;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="utilisateur")
     */
    private $reglements;

    /**
     * @ORM\OneToOne(targetEntity=ConfirmTokenMail::class, mappedBy="utilisateur")
     */
    private $confirmTokenMail;

    /**
     * @ORM\OneToOne(targetEntity=PasswordResetToken::class, mappedBy="utilisateur",)
     */
    private $passwordResetToken;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @var string
     */
    private $googleauthenticatorsecret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $googleId;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $googleauthenticatorenable = false;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $balance = 0;


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Souscription[]
     */
    public function getSouscriptions(): Collection
    {
        return $this->souscriptions;
    }

    /**
     * @return Collection|Entreprise[]
     */
    public function getEntreprises(): Collection
    {
        return $this->entreprises;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Utilisateur
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string $username
     * @return Utilisateur
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param string $password
     * @return Utilisateur
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    /**
     * @param int $telephone
     * @return Utilisateur
     */
    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateNaissance(): ?DateTime
    {
        return $this->dateNaissance;
    }

    /**
     * @param DateTime $dateNaissance
     * @return Utilisateur
     */
    public function setDateNaissance(DateTime $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastConnexion(): ?DateTime
    {
        return $this->lastConnexion;
    }

    /**
     * @param DateTime $lastConnexion
     * @return Utilisateur
     */
    public function setLastConnexion(DateTime $lastConnexion): Utilisateur
    {
        $this->lastConnexion = $lastConnexion;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastDeconnexion(): ?DateTime
    {
        return $this->lastDeconnexion;
    }

    /**
     * @param DateTime $lastDeconnexion
     * @return Utilisateur
     */
    public function setLastDeconnexion(DateTime $lastDeconnexion): Utilisateur
    {
        $this->lastDeconnexion = $lastDeconnexion;
        return $this;
    }

    /**
     * @return Role
     */
    public function getRole(): ?Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return Utilisateur
     */
    public function setRole(Role $role): self
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActif(): ?bool
    {
        return $this->actif;
    }

    /**
     * @param bool $actif
     * @return Utilisateur
     */
    public function setActif(bool $actif): self
    {
        $this->actif = $actif;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleauthenticatorsecret(): ?string
    {
        return $this->googleauthenticatorsecret;
    }

    /**
     * @param string $googleauthenticatorsecret
     * @return Utilisateur
     */
    public function setGoogleauthenticatorsecret(string $googleauthenticatorsecret): self
    {
        $this->googleauthenticatorsecret = $googleauthenticatorsecret;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     * @return Utilisateur
     */
    public function setGoogleId(string $googleId): self
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGoogleauthenticatorenable(): ?bool
    {
        return $this->googleauthenticatorenable;
    }

    /**
     * @param bool $googleauthenticatorenable
     * @return Utilisateur
     */
    public function setGoogleauthenticatorenable(
        bool $googleauthenticatorenable): self
    {
        $this->googleauthenticatorenable = $googleauthenticatorenable;
        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): ?int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     * @return Utilisateur
     */
    public function setBalance(int $balance): self
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return ConfirmTokenMail
     */
    public function getConfirmTokenMail(): ?ConfirmTokenMail
    {
        return $this->confirmTokenMail;
    }

    /**
     * @param ConfirmTokenMail $confirmTokenMail
     * @return Utilisateur
     */
    public function setConfirmTokenMail(ConfirmTokenMail $confirmTokenMail)
    {
        $this->confirmTokenMail = $confirmTokenMail;
        return $this;
    }

    /**
     * @return PasswordResetToken
     */
    public function getPasswordResetToken(): ?PasswordResetToken
    {
        return $this->passwordResetToken;
    }

    /**
     * @param PasswordResetToken $passwordResetToken
     * @return Utilisateur
     */
    public function setPasswordResetToken(
        PasswordResetToken $passwordResetToken): self
    {
        $this->passwordResetToken = $passwordResetToken;
        return $this;
    }



    /**
     * @param $id
     * @return Collection|Recette[]
     */
    public function getRecettes($id): Collection
    {
        return $this->recettes->filter(static function ($recette) use($id){
            return $recette->getEntreprise()->getId() == $id;
        });
    }

    /**
     * @param $id
     * @return Collection|Depense[]
     */
    public function getDepenses($id): Collection
    {
        return $this->depenses->filter(static function ($depense) use($id){
            return $depense->getEntreprise()->getId() == $id;
        });
    }

    /**
     * @var array|string[]
     * @return array
     */
    public function getRoles(): ?array
    {
        return [$this->role->getName()];
    }


    /**
     * @param $id
     * @return Collection|Dette[]
     */
    public function getDettes($id): Collection
    {
        return $this->dettes->filter(static function ($dette) use($id){
            return $dette->getEntreprise()->getId() == $id;
        });
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function isEnabled(): ?bool
    {
        return $this->actif;
    }

    public function getDayRemainingBirthDay(): ?int
    {
        $dateBirthDay = date_create($this->getDateRecreated($this->dateNaissance)
            ->format('Y-m-d'));
        $dateToday = new DateTime();
        $today = date_create($dateToday->format('Y-m-d'));
        return $dateBirthDay->diff($today)->days;
    }

    // recreate date with the new year
    public function getDateRecreated(DateTime $date): DateTime
    {
        $formatedDate = strtotime($date->format('Y-m-d'));
        return date_date_set(
            $date,
            date('Y'),
            date('n',$formatedDate),
            date('d',$formatedDate)
        );
    }

    public function dateisUpThanToday(): ?bool
    {
        return $this->getDateRecreated($this->dateNaissance) > new DateTime();
    }

    /**
     * @param $id
     * @return Collection|Reglement[]
     */
    public function getReglements($id): Collection
    {
        return $this->reglements->filter(static function ($reglement) use($id){
            return $reglement->getDette()->getEntreprise()->getId() == $id;
        });
    }

    /**
     * @inheritDoc
     */
    public function isEqualTo(UserInterface $user): ?bool
    {
        return $this->getEmail() === $user->getEmail();
    }
}
