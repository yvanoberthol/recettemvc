<?php


namespace App\Entity\form;

use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * Class RegisterForm
 * @package App\Entity\form
 */
class RegisterForm
{

    /**
     * @Assert\Length(min=4,minMessage="Le nom d'utilisateur doit contenir au moins 4 caractères")
     * @var string
     */
    private $username;

    /**
     * @Assert\Email(message="Adresse Email invalide")
     * @var string
     */
    private $email;

    /**
     * @Assert\Positive(message="Numéro de téléphone invalide")
     * @var integer
     */
    private $telephone;

    /**
     * @Assert\Length(min=6,minMessage="Le mot de passe doit contenir au moins 6 caractères")
     * @var string
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password",message="Mot de passe de confirmation incorrect")
     * @var string
     */
    private $confirmPassword;

    /**
     * @Assert\Type("\DateTimeInterface",message="Entrez une date valide")
     * @var DateTime
     */
    private $dateNaissance;

    /**
     * @Assert\Length(min=4,minMessage="Le nom de votre organisation doit contenir au moins 4 caractères")
     * @var string
     */
    private $entrepriseName;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return RegisterForm
     */
    public function setEmail(string $email): RegisterForm
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    /**
     * @param int $telephone
     * @return RegisterForm
     */
    public function setTelephone(int $telephone): RegisterForm
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RegisterForm
     */
    public function setPassword(string $password): RegisterForm
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string $confirmPassword
     * @return RegisterForm
     */
    public function setConfirmPassword(string $confirmPassword): RegisterForm
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateNaissance(): ?DateTime
    {
        return $this->dateNaissance;
    }

    /**
     * @param DateTime $dateNaissance
     * @return RegisterForm
     */
    public function setDateNaissance(DateTime $dateNaissance): RegisterForm
    {
        $this->dateNaissance = $dateNaissance;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntrepriseName(): ?string
    {
        return $this->entrepriseName;
    }

    /**
     * @param string $entrepriseName
     * @return RegisterForm
     */
    public function setEntrepriseName(string $entrepriseName): RegisterForm
    {
        $this->entrepriseName = $entrepriseName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return RegisterForm
     */
    public function setUsername(string $username): RegisterForm
    {
        $this->username = $username;
        return $this;
    }
}