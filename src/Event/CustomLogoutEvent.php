<?php


namespace App\Event;

use App\Entity\Utilisateur;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class CustomLogoutEvent implements LogoutSuccessHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * CustomLogoutEvent constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router)
    {

        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     * @return RedirectResponse never null
     * @throws Exception
     */
    public function onLogoutSuccess(Request $request)
    {
        // TODO: Implement onLogoutSuccess() method.
        // TODO: Implement logout() method.

        if ($this->tokenStorage->getToken() !== null){

            $userRepository = $this->entityManager->getRepository(Utilisateur::class);
            $utilisateur = $userRepository->findOneBy(
                [
                    'username'=> $this->tokenStorage->getToken()->getUsername()

                ]
            );

            if ($utilisateur !== null) {
                $utilisateur->setLastDeconnexion(new DateTime());
                $this->entityManager->persist($utilisateur);
                $this->entityManager->flush();
            }
        }

        $route =  $this->router->generate('account_connexion');
        return new RedirectResponse($route);
    }
}