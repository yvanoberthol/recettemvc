<?php


namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FrenchToDateTimeTransformer implements DataTransformerInterface
{

    /**
     * @inheritDoc
     */
    public function transform($date)
    {
        return ($date === null)?'':$date->format('d/m/Y');
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($frenchDate)
    {
        if ($frenchDate === null){
            throw new TransformationFailedException('vous devez entrer une date');
        }

        $date = \DateTime::createFromFormat('d/m/Y', $frenchDate);

        if ($date === false){
            throw new TransformationFailedException("Le format de la date n'est pas le bon");
        }

        return $date;
    }
}