<?php

namespace App\Form;

use App\Entity\Depense;
use App\Entity\Produit;
use App\Repository\DepenseRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepenseType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;


    /**
     * RecetteType constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',DateType::class,[
                'label' => 'form.depense.date.title',
                'widget' => 'single_text'
            ])
            ->add('montant',IntegerType::class,[
                'label' => 'form.depense.montant.title',
                'attr' => [
                    'placeholder' => '0'
                ]
            ])
            ->add('raison',TextareaType::class,[
                'label' => 'form.depense.raison.title',
                'attr' => [
                    'placeholder' => 'form.depense.raison.placeholder'
                ]
            ])
            ->add('produit', EntityType::class,[
                'class' => Produit::class,
                'label' => 'form.depense.produit.title',
                'choice_label' => 'name',
                'query_builder' => function(ProduitRepository $er) {
                    return $er->queryOwnedBy($this->session->get('entreprise'));
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Depense::class,
        ]);
    }
}
