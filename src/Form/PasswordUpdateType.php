<?php

namespace App\Form;

use App\Entity\PasswordUpdate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldpassword',
                PasswordType::class,[
                    'label' => 'form.passwordUpdate.oldpassword.title',
                    'attr' => [
                        'placeholder' => 'form.passwordUpdate.oldpassword.placeholder'
                    ]
                ])
            ->add('newpassword',PasswordType::class,[
                'label' => 'form.passwordUpdate.newpassword.title',
                'attr' => [
                    'placeholder' => 'form.passwordUpdate.newpassword.placeholder'
                ]
            ])
            ->add('confirmpassword',
                PasswordType::class,[
                    'label' => 'form.passwordUpdate.confirmpassword.title',
                    'attr' => [
                        'placeholder' => 'form.passwordUpdate.confirmpassword.placeholder'
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            "data_class" => PasswordUpdate::class
        ]);
    }
}
