<?php

namespace App\Form;

use App\Entity\Produit;
use App\Entity\Recette;
use App\Repository\ProduitRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecetteType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;


    /**
     * RecetteType constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',DateType::class,[
                'label' => 'form.recette.date.title',
                'widget' => 'single_text'
            ])
            ->add('montant',IntegerType::class,[
                'label' => 'form.recette.montant.title',
                'attr' => [
                    'placeholder' => '0'
                ]
            ])
            ->add('produit', EntityType::class,[
                'label' => 'form.recette.produit.title',
                'class' => Produit::class,
                'choice_label' => 'name',
                'query_builder' => function(ProduitRepository $er) {
                    return $er->queryOwnedBy($this->session->get('entreprise'));
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recette::class,
        ]);
    }
}
