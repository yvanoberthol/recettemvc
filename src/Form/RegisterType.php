<?php

namespace App\Form;

use App\Entity\form\RegisterForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',TextType::class,[
                'label' => 'form.register.username.title',
                'attr' => [
                    'placeholder' => 'form.register.username.placeholder'
                ]
            ])
            ->add('password',PasswordType::class,[
                'label' => 'form.register.password.title',
                'attr' => [
                    'placeholder' => 'form.register.password.placeholder'
                ]
            ])
            ->add('confirmPassword',PasswordType::class,[
                'label' => 'form.register.confirmpassword.title',
                'attr' => [
                    'placeholder' => 'form.register.confirmpassword.placeholder'
                ]
            ])
            ->add('email',EmailType::class,[
                'label' => 'form.register.email.title',
                'attr' => [
                    'placeholder' => 'form.register.email.placeholder'
                ]
            ])
            ->add('telephone',TextType::class,[
                'label' => 'form.register.telephone.title',
                'attr' => [
                    'placeholder' => 'form.register.telephone.placeholder'
                ]
            ])
            ->add('dateNaissance',DateType::class, [
                'label' => 'form.register.dateNaissance.title',
                'widget' => 'single_text'
            ])
            ->add('entrepriseName',TextType::class, [
                'label' => 'form.register.entrepriseName.title',
                'attr' => [
                    'placeholder' => 'form.register.entrepriseName.placeholder'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegisterForm::class,
        ]);
    }
}
