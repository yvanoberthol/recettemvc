<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function queryOwnedBy($entreprise): QueryBuilder
    {

        $query = $this->createQueryBuilder('c')
            ->innerJoin('c.entreprise', 'e')
            ->where('e.id = :id')
            ->setParameter('id', $entreprise);

        return $query;
    }

    public function findOwnedBy($entreprise) {
        return $this->queryOwnedBy($entreprise)
            ->getQuery()
            ->getResult();
    }
}
