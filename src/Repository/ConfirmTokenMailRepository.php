<?php

namespace App\Repository;

use App\Entity\ConfirmTokenMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConfirmTokenMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfirmTokenMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfirmTokenMail[]    findAll()
 * @method ConfirmTokenMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfirmTokenMailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfirmTokenMail::class);
    }

    // /**
    //  * @return ConfirmTokenMail[] Returns an array of ConfirmTokenMail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConfirmTokenMail
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
