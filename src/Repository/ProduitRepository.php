<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    public function queryOwnedBy($entreprise): QueryBuilder
    {

        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.entreprise', 'e')
            ->where('e.id = :id')
            ->setParameter('id', $entreprise);

        return $query;
    }

    public function findOwnedBy($entreprise) {
        return $this->queryOwnedBy($entreprise)
            ->getQuery()
            ->getResult();
    }
}
