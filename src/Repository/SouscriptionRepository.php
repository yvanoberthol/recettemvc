<?php

namespace App\Repository;

use App\Entity\Souscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Souscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Souscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Souscription[]    findAll()
 * @method Souscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SouscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Souscription::class);
    }
}
