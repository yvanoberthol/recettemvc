<?php


namespace App\Security;


use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class CustomAuthenticator extends AbstractGuardAuthenticator
{
    use TargetPathTrait;

    public const loginRoute = 'account_connexion';
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var UtilisateurProvider
     */
    private $customerProvider;


    /**
     * DatabaseAuthenticator constructor.
     * @param RouterInterface $router
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UtilisateurProvider $customerProvider
     */
    public function __construct(RouterInterface $router, CsrfTokenManagerInterface $csrfTokenManager,
                                UserPasswordEncoderInterface $passwordEncoder, UtilisateurProvider $customerProvider)
    {
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->customerProvider = $customerProvider;
    }

    public function supports(Request $request)
    {
        return self::loginRoute === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = $this->router->generate('account_connexion');

        return new RedirectResponse($url);
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->get('_username'),
            'password' => $request->get('_password'),
            'csrf_token' => $request->get('_csrf_token')
        ];

        if ($request->get('_entreprise_id') !== 0){
            $credentials['entreprise_id'] = $request->get('_entreprise_id');
            $request->getSession()->set('entreprise',$credentials['entreprise_id']);
        }

        $request->getSession()->set(Security::LAST_USERNAME, $credentials['username']);

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        return $this->customerProvider->loadUserByUsername($credentials['username']);
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user,$credentials['password']);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(),$providerKey)){
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate('account_two_factor'));
    }

    public function supportsRememberMe()
    {
        return true;
    }
}