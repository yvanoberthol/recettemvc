<?php


namespace App\Security;


use App\Entity\Utilisateur as User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserChecker constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @inheritDoc
     */
    public function checkPreAuth(UserInterface $user)
    {
        // TODO: Implement checkPreAuth() method.
        if (!$user instanceof User){
            return;
        }

        if (!$user->isEnabled()){
            throw new CustomMessageSecurity('Votre compte a été désactivé');
        }
    }

    /**
     * @inheritDoc
     */
    public function checkPostAuth(UserInterface $user)
    {
        // TODO: Implement checkPreAuth() method.
        if (!$user instanceof User){
            return;
        }

        if (!$user->isEnabled()){
            throw new CustomMessageSecurity('Votre compte a été désactivé');
        }

        $user->setLastConnexion(new DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}