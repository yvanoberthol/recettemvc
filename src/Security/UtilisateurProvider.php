<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 22/06/19
 * Time: 10:12
 */

namespace App\Security;


use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UtilisateurProvider implements UserProviderInterface
{
    /**
     * @var UtilisateurRepository
     */
    private $utilisateurRepository;


    /**
     * UtilisateurProvider constructor.
     * @param UtilisateurRepository $utilisateurRepository
     */
    public function __construct(UtilisateurRepository $utilisateurRepository)
    {
        $this->utilisateurRepository = $utilisateurRepository;
    }

    public function refreshUser(UserInterface $user, $safe = false)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function loadUserByUsername($login)
    {
        return $this->utilisateurRepository->findOneBy([
            "username" => $login
        ]);
    }

    public function supportsClass($class)
    {
        return $class === Utilisateur::class;
    }
}