<?php


namespace App\Service\BirthDayEvent;


use Symfony\Component\HttpFoundation\Response;

class AddBirthDayHtml
{
    public function addBirthDay(Response $response, array $users): Response
    {
        $strBirthDay = 'Aniversaire(s) proche(s): [ ';
        foreach($users as $user){
          $strBirthDay .=$user->getUsername().': <span class="text-warning">JJ-'.$user->getDayRemainingBirthDay().'</span>  ';
        }
        $strBirthDay .=']';
        $content = $response->getContent();
        $htmlBirthDay = '<div class="row bg-info text-white py-3 font-weight-bolder h5"><div class="col-md-12 text-center">'.$strBirthDay.'</div></div>';
        $replaceString = '<div class="col-md-12 birthday text-center">';
        $content = str_replace($replaceString,$replaceString.$htmlBirthDay,$content);

        $response->setContent($content);
        return $response;
    }
}