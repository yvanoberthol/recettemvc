<?php


namespace App\Service\BirthDayEvent;


use App\Entity\Entreprise;
use App\Entity\Parametre;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class BirthDayListener
{
    protected $birthDayHtml;

    private $entityManager;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * BookerDayListener constructor.
     * @param AddBirthDayHtml $birthDayHtml
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     */
    public function __construct(
        AddBirthDayHtml $birthDayHtml,
        EntityManagerInterface $entityManager,
        SessionInterface $session)
    {
        $this->birthDayHtml = $birthDayHtml;
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public function processHtmlBirthDay( ResponseEvent $event): void
    {
        if (!$event->isMasterRequest() || $this->session->get('entreprise') === null){
            return ;
        }

        $listUsers = [];

        $entreprise = $this->entityManager->getRepository(Entreprise::class)
            ->find($this->session->get('entreprise'));

        $parametre = $this->entityManager->getRepository(Parametre::class)
            ->findOneBy(['code' => 'birthday_remaining']);

        if ($parametre === null || $entreprise === null)
            return ;

        $utilisateurs = $entreprise->getUtilisateurs();
        foreach ($utilisateurs as $utilisateur) {
            $remainingDays = $utilisateur->getDayRemainingBirthDay();

            if ($utilisateur->dateisUpThanToday()) {
                if ($remainingDays > 0 && $remainingDays <= $parametre->getValue())
                    $listUsers[] = $utilisateur;
            }

        }

        if ($listUsers) {
            $reponse = $this->birthDayHtml->addBirthDay($event->getResponse(), $listUsers);
            $event->setResponse($reponse);
        }
    }
}