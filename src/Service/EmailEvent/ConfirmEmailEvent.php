<?php


namespace App\Service\EmailEvent;


use App\Entity\Utilisateur;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ConfirmEmailEvent extends EventDispatcher
{
    /**
     * @var Utilisateur
     */
    private $utilisateur;

    /**
     * ConfirmEmailEvent constructor.
     * @param Utilisateur $utilisateur
     */
    public function __construct(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): Utilisateur
    {
        return $this->utilisateur;
    }
}