<?php


namespace App\Service\EmailEvent;


class ConfirmEmailListener
{
    /**
     * @var ConfirmEmailSender
     */
    private $confirmEmailSender;

    public function __construct(ConfirmEmailSender $confirmEmailSender)
    {
        $this->confirmEmailSender = $confirmEmailSender;
    }

    public function processConfirmMail(ConfirmEmailEvent $confirmEmailEvent)
    {
        $this->confirmEmailSender->createConfirmationMail($confirmEmailEvent->getUtilisateur());
    }
}