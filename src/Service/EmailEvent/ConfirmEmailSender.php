<?php


namespace App\Service\EmailEvent;


use App\Entity\ConfirmTokenMail;
use App\Entity\Util\StringUtil;
use App\Entity\Utilisateur;
use App\Service\SendMailer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ConfirmEmailSender
{

    /**
     * @var SendMailer
     */
    private $sendMailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager,SendMailer $sendMailer)
    {
        $this->sendMailer = $sendMailer;
        $this->entityManager = $entityManager;
    }

    public function createConfirmationMail(Utilisateur $utilisateur)
    {
        if ($utilisateur->getConfirmTokenMail() === null){
            $confirmMailToken = new ConfirmTokenMail();
            $confirmMailToken->setToken(StringUtil::randomString(50));
            $confirmMailToken->setCreatedAt(new DateTime());
            $confirmMailToken->setUtilisateur($utilisateur);
            $this->entityManager->persist($confirmMailToken);
            $this->entityManager->flush();
        }

        $this->sendMailer->sendConfirmationEmail($utilisateur);
    }
}