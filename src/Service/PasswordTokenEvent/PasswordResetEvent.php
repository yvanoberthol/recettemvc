<?php


namespace App\Service\PasswordTokenEvent;


use App\Entity\Utilisateur;
use Symfony\Component\EventDispatcher\EventDispatcher;

class PasswordResetEvent extends EventDispatcher
{
    /**
     * @var Utilisateur
     */
    private $utilisateur;

    public function __construct(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): Utilisateur
    {
        return $this->utilisateur;
    }
}