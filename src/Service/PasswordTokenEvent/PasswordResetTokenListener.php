<?php


namespace App\Service\PasswordTokenEvent;



class PasswordResetTokenListener
{
    /**
     * @var PasswordResetTokenSender
     */
    private $passwordResetTokenSender;

    public function __construct(PasswordResetTokenSender $passwordResetTokenSender)
    {
        $this->passwordResetTokenSender = $passwordResetTokenSender;
    }

    public function processPasswordResetToken(PasswordResetEvent $passwordResetEvent)
    {
        $this->passwordResetTokenSender
            ->createPasswordResetTokenMail($passwordResetEvent->getUtilisateur());
    }
}