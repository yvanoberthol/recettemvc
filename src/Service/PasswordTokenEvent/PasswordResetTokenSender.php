<?php


namespace App\Service\PasswordTokenEvent;


use App\Entity\ConfirmTokenMail;
use App\Entity\PasswordResetToken;
use App\Entity\Util\StringUtil;
use App\Entity\Utilisateur;
use App\Service\SendMailer;
use Doctrine\ORM\EntityManagerInterface;

class PasswordResetTokenSender
{

    /**
     * @var SendMailer
     */
    private $sendMailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager,SendMailer $sendMailer)
    {
        $this->sendMailer = $sendMailer;
        $this->entityManager = $entityManager;
    }

    public function createPasswordResetTokenMail(Utilisateur $utilisateur)
    {
        if ($utilisateur->getPasswordResetToken() === null){
            $passwordResetToken = new PasswordResetToken();
            $passwordResetToken->setToken(StringUtil::randomString(50));
            $passwordResetToken->setCreatedAt(new \DateTime());
            $passwordResetToken->setUtilisateur($utilisateur);
            $this->entityManager->persist($passwordResetToken);
            $this->entityManager->flush();
        }

        $this->sendMailer->sendPasswordResetTokenMail($utilisateur);
    }
}