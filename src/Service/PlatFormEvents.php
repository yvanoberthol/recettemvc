<?php


namespace App\Service;


final class PlatFormEvents
{
    public const CREATE_SOUSCRIPTION_DEFAULT = 'create_souscription.default';
    public const CREATE_CONFIRM_MAIL = 'create_confirm_mail.default';
    public const CREATE_LINK_PASSWORD_TOKEN = 'create_link_password_token.default';

}