<?php


namespace App\Service;


use App\Entity\Utilisateur;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Swift_Mailer;

class SendMailer
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    private const ADRESSE_EMAIL_SENDER = 'marieemmagam@gmail.com';
    private const LINK = 'http://localhost:8000/';


    /**
     * SendMailer constructor.
     * @param Swift_Mailer $mailer
     */
    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    // permet d'envoyer les identifiants de connexion à l'utilisateur
    public function sendIdentifier($login,$password): void
    {
        $validator = new EmailValidator();
        $body = "votre login : $login et votre mot de passe: $password";

        if ($validator->isValid($login,new RFCValidation())) {

            $message = (new \Swift_Message())->setFrom(self::ADRESSE_EMAIL_SENDER)
                ->setTo($login)
                ->setSubject('Vos identifiants de connexion')
                ->setBody($body);

            $this->mailer->send($message);
        }
    }

    public function sendConfirmationEmail(Utilisateur $utilisateur): void
    {
        $confirmEmailToken = $utilisateur->getConfirmTokenMail();

        if ($confirmEmailToken !== null){
            $link = self::LINK.'confirmationMail?token='.$confirmEmailToken->getToken();
            $validator = new EmailValidator();
            $body = "Activez votre compte via ce lien: ".$link;

            if ($validator->isValid($utilisateur->getEmail(),new RFCValidation())) {

                $message = (new \Swift_Message())->setFrom(self::ADRESSE_EMAIL_SENDER)
                    ->setTo($utilisateur->getEmail())
                    ->setSubject('Vérification du compte créé')
                    ->setBody($body);

                $this->mailer->send($message);
            }
        }
    }

    public function sendPasswordResetTokenMail(Utilisateur $utilisateur): void
    {
        $passwordResetToken = $utilisateur->getPasswordResetToken();

        if ($passwordResetToken !== null){
            $link = self::LINK.'passwordResetTokenMail?token='.$passwordResetToken->getToken();
            $validator = new EmailValidator();
            $body = "Lien pour modification du mot passe: ".$link;

            if ($validator->isValid($utilisateur->getEmail(),new RFCValidation())) {

                $message = (new \Swift_Message())->setFrom(self::ADRESSE_EMAIL_SENDER)
                    ->setTo($utilisateur->getEmail())
                    ->setSubject('Mot de passe oublié')
                    ->setBody($body);

                $this->mailer->send($message);
            }
        }
    }
}