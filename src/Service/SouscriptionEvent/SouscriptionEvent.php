<?php


namespace App\Service\SouscriptionEvent;


use App\Entity\Entreprise;
use App\Entity\Utilisateur;
use Symfony\Contracts\EventDispatcher\Event;

class SouscriptionEvent extends Event
{

    /**
     * @var Utilisateur
     */
    private $utilisateur;
    /**
     * @var Entreprise
     */
    private $entreprise;

    public function __construct(Utilisateur $utilisateur, Entreprise $entreprise)
    {
        $this->utilisateur = $utilisateur;
        $this->entreprise = $entreprise;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): Entreprise
    {
        return $this->entreprise;
    }

}