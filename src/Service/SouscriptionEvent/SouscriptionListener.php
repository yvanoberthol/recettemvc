<?php


namespace App\Service\SouscriptionEvent;


class SouscriptionListener
{

    /**
     * @var SouscriptionRegisterDefault
     */
    private $registerDefault;

    public function __construct(SouscriptionRegisterDefault $registerDefault)
    {
        $this->registerDefault = $registerDefault;
    }

    public function processCreateSouscription(SouscriptionEvent $souscriptionEvent): void
    {
        $this->registerDefault
            ->createSouscription(
                $souscriptionEvent->getUtilisateur(),
                $souscriptionEvent->getEntreprise()
            );
    }

}