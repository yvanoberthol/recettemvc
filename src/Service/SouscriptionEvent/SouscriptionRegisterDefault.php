<?php


namespace App\Service\SouscriptionEvent;


use App\Entity\Entreprise;
use App\Entity\Package;
use App\Entity\Souscription;
use App\Entity\Utilisateur;
use App\Service\SendMailer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class SouscriptionRegisterDefault
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SendMailer
     */
    private $sendMailer;

    public function __construct(EntityManagerInterface $entityManager,
                                SendMailer $sendMailer)
    {
        $this->entityManager = $entityManager;
        $this->sendMailer = $sendMailer;
    }

    public function createSouscription(Utilisateur $utilisateur, Entreprise $entreprise)
    {

        $planRepository = $this->entityManager->getRepository(Package::class);
        $plan = $planRepository->findOneBy(['nbreJours' => 30]);

        $souscription = new Souscription();
        $souscription->setDate(new DateTime());
        $souscription->setCreatedAt(new DateTime());
        $souscription->setPackage($plan);
        $souscription->setEntreprise($entreprise);
        $souscription->setUtilisateur($utilisateur);

        $this->entityManager->persist($souscription);
        $this->entityManager->flush();
    }

}