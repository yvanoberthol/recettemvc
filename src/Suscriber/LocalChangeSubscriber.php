<?php


namespace App\Suscriber;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class LocalChangeSubscriber implements EventSubscriberInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * SouscriptionSubscriber constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['processLocalChange', -5],
        ];
    }

    public function processLocalChange(RequestEvent $event): void
    {
        if (getenv('APP_ENV') === 'prod'){
            $langues = ['fr', 'en'];
            $request = $event->getRequest();
            $path = $request->getPathInfo();
            $lang = $request->get('_locale');

            $routeMatches = $this->router->getRouteCollection()->all();
            $routes = [];
            foreach ($routeMatches as $route){
                if (str_contains($route->getPath(),'_locale'))
                    $routes[] = substr($route->getPath(),10);
            }

            if (!in_array($lang, $langues, true)){
                if (!empty($routes) && in_array($path, $routes, true)){
                    $local = ($lang) ?: 'fr';
                    $route = '/'.$local.$path;
                    $response = new RedirectResponse($route);
                    $event->setResponse($response);
                }else{
                    $local = ($lang) ?: 'fr';
                    $response = new RedirectResponse('/'.$local);
                    $event->setResponse($response);
                }
            }
        }
    }
}