<?php


namespace App\Suscriber;


use App\Entity\Entreprise;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class SouscriptionSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    public function __construct(EntityManagerInterface $entityManager,
                                SessionInterface $session,
                                RouterInterface $router,
                                FlashBagInterface $flashBag)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->router = $router;
        $this->flashBag = $flashBag;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['processExpirationSubscription', -8],
        ];
    }

    public function processExpirationSubscription(RequestEvent $event): void
    {
        $allowedPaths = [
            '/admin/login',
            '/admin/logout',
            '/admin/',
            '/admin/souscription/renew',
        ];

        // use substring to remove lang on the url
        $path = substr($event->getRequest()->getPathInfo(),3);

        if (!in_array($path, $allowedPaths, true) &&
            strpos($path, '/admin/') !== false &&
            $this->session->get('entreprise') !== null) {
            $entrepriseRepository = $this->entityManager->getRepository(Entreprise::class);
            $entreprise = $entrepriseRepository->find($this->session->get('entreprise'));

            if (($entreprise !== null) &&
                ($souscription = $entreprise->getSouscription()) &&
                $souscription->getNbreJourRestants() === 0) {
                $this->flashBag->clear();
                $this->flashBag->add('warning', 'flash.souscription.expiration_plan');
                $url = $this->router->generate('accueil');
                $response = new RedirectResponse($url);
                $event->setResponse($response);
            }
        }
    }
}