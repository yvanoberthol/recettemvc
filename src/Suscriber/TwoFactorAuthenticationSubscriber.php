<?php


namespace App\Suscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class TwoFactorAuthenticationSubscriber implements EventSubscriberInterface
{

    public const ROLE_2FA_SUCCEED = '2FA_SUCCEED';

    private const FIREWALL_NAME = 'admin';
    private const ROUTE_FOR_2FA = 'account_two_factor';
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * TwoFactorAuthenticationSubscriber constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     */
    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }


    public function onKernelRequest(RequestEvent $event): void
    {

        if (!$event->isMasterRequest()) {
            return;
        }

        $disallowedPaths = [
            '/admin/login',
            '/admin/two_factor',
        ];

        $path = $event->getRequest()->getPathInfo();

        if (in_array($path, $disallowedPaths, true)){
            return;
        }


        if (($currentToken = $this->tokenStorage->getToken()) &&
            $currentToken instanceof PostAuthenticationGuardToken &&
            ($currentToken->getProviderKey() === self::FIREWALL_NAME) &&
            !$this->hasRole($currentToken, self::ROLE_2FA_SUCCEED) &&
            $currentToken->getUser()->isGoogleauthenticatorenable()) {
            $response = new RedirectResponse($this->router->generate(self::ROUTE_FOR_2FA));
            $event->setResponse($response);
        }

    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', -10],
        ];
    }

    private function hasRole(TokenInterface $token, string $role): bool
    {
        foreach ($token->getRoleNames() as $userRole) {
            if ($userRole === $role) {
                return true;
            }
        }
        return false;
    }
}